// Creates a fadein/fadeout affect to refresh occupation rate in
// Activity main view
function afterVehicleDelete(link,success,data) 
{
	if(success)
	{ 
		// Refresh occupation info
		$.fn.yiiGridView.update("pax-activity-grid");
		
		// refresh available vehicles grid
		$.fn.yiiGridView.update("available-vehicle-grid");
	}
	
	return false;
}

// Refresh available staff grid after removing staff from related staff grid
function afterStaffDelete(link, success, data)
{
	if (success)
		$.fn.yiiGridView.update("available-staff-grid");
		
	return false;

}

function getTotalPayed()
{
	// Get total cash when value payed adults loses focus
	$("#Client_value_payed_children").focusout(function(){
		calculateTotalCash();
	});
	
	// Get total cash when value payed adults loses focus
	$("#Client_value_payed_adult").focusout(function(){
		calculateTotalCash();
	});
	
	$("#Client_pax").focusout(function(){
		calculateTotalCash();
	});
	
	$("#Client_number_children").focusout(function(){
		calculateTotalCash();
	});
	
	// Get total cash when document loads
	$(document).ready(function(){
		calculateTotalCash()
	});
	
	function calculateTotalCash()
	{
		var valueChildren = $('#Client_value_payed_children').val();
		var children = $('#Client_number_children').val();
		
		var valueAdults = $('#Client_value_payed_adult').val();
		var adults = $('#Client_pax').val();

		var total = parseFloat(valueChildren) + parseFloat(valueAdults);
		var total = (parseFloat(valueChildren) * parseInt(children)) +
			parseFloat(valueAdults) * parseInt(adults);
		$('#total_cash').html("<b>Total Cash</b>: "+total);
	}

}
