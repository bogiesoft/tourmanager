///////// Instalation //////////


* Create a database in MySQL that will be your application database.

* Download the Yii framework from http://www.yiiframework.com/download and extract it
	into the desired folder (e.g. /var/www/html/webapp).

* Extract the tourmanager file and copy the folder into the folder you have extracted
	the Yii framework (e.g. /var/www/html/webapp/tourmanager).

* Open tourmanager/protected/config/main.php and fill in your databse server, 
	database name	and username and password in the 'db' component.
	
* Edit the admin email address in protected/config/main.php in the 'adminEmail'
	component.
	
* Import the database tourmanager.sql schema located in 
	tourmanager/protected/data folder to your database.

* Navigate to http://yourdomain/tourmanager and login with the default username
	and password wich is admin for both fields. Be sure to change the password 
	imediatly.
	
* NOTE: Do NOT change the admin username. This is used for the permission 
	management module. To change the username you first need to edit the 
	'superuserName' in protected/config/main.php to the username you want to
	use as administrator.
