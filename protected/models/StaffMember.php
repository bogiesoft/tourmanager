<?php

/**
 * This is the model class for table "tbl_staff_member".
 *
 * The followings are the available columns in table 'tbl_staff_member':
 * @property integer $id
 * @property string $name
 * @property integer $freelancer
 * @property string $create_time
 * @property string $update_time
 * @property integer $role_id
 *
 * The followings are the available model relations:
 * @property StaffRole $role0
 */
class StaffMember extends TourManagerActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return StaffMember the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_staff_member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('role_id, freelancer', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>64),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, freelancer, create_time, update_time, role_id, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'role' => array(self::BELONGS_TO, 'StaffRole', 'role_id'),
			'activities'=>array(self::MANY_MANY, 'Activity',
				'rel_activity_staff(activity_id, staff_member_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'freelancer' => 'Freelancer',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'role_id' => 'Role',
			'roleName' => 'Role',
			'freelancerStatus' => 'Freelancer'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('freelancer',$this->freelancer);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return the name of the role of this staff member
	 */
	public function getRoleName()
	{
		if($this->role)
		  return $this->role->name;
	}

	/**
	  * Associates a StaffMember with Activity by populating rel_activity_staff table
	  * @param int $actvitiyId activity id to associate
	  * @return true if successfull, false otherwise
	  */
	 public function associateStaffWithActivity($activityId)
	 {
	 	$sql = "INSERT INTO rel_activity_staff (activity_id, staff_member_id) " .
	 			"VALUES (:activityId, :staffId)";

	 	$command = Yii::app()->db->createCommand($sql);
	 	$command->bindValue(":activityId", $activityId, PDO::PARAM_INT);
	 	$command->bindValue(":staffId", $this->id, PDO::PARAM_INT);

	 	// Catch exception when inserting existing relation
	 	try
	 	{
	 		$command->execute();
	 	} catch(CDbException $e)
	 	{
	 		return false;
	 	}

	 	return true;
	 }

	 /**
	  * Removes a relation of an Activity with a vehicle
	  * @param $actviityId the id of the activity to disassociate
	  * @return true if query is sucessfull, false otherwise
	  */
	 public function disassociateStaffWithActivity($activityId)
	 {
	 	$sql = "DELETE FROM rel_activity_staff WHERE activity_id = :activityId" .
	 			" AND staff_member_id = :staffId";

	 	$command = Yii::app()->db->createCommand($sql);
	 	$command->bindValue(":activityId", $activityId, PDO::PARAM_INT);
	 	$command->bindValue(":staffId", $this->id, PDO::PARAM_INT);

	 	// Catch exception when inserting deleting relation
	 	try
	 	{
	 		$command->execute();
	 	} catch(CDbException $e)
	 	{
	 		return false;
		}

		return true;
	 }

	/**
	 * @return string representation of freelancer status
	 */
	public function getFreelancerStatus()
	{
	  if ($this->freelancer == 1)
	    return 'Yes';

	  return 'No';
	}
}