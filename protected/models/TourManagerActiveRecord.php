<?php
/*
 * Created on 16 de Jun de 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 abstract class TourManagerActiveRecord extends CActiveRecord
 {
 	
 	protected function beforeValidate()
 	{
 		if ($this->isNewRecord)
 		{
 			//set create time and updated time
 			$this->create_time = $this->update_time = 
 				new CDbExpression('NOW()');
 		}
 		
 		else
 		{
 			//set last update time
 			$this->update_time = $this->update_time = 
 				new CDbExpression('NOW()');
 		}
 		return parent::beforeValidate();
 	}
 	
 	/**
 	 * @param useIds boolean paramenter check ids should be used has
 	 *  keys for the return array
	 * @return array with names of all records
	 */
	public function getAllNames($useIds = false)
	{
		$instances = self::findAll();
		$instanceNames = array();
		
		if ($useIds === true)
			foreach($instances as $instance)
				$instanceNames[$instance->id] = $instance->name;
				
		else
			foreach($instances as $instance)
				$instanceNames[] = $instance->name;
		
		return $instanceNames;
	}
  
 }
 
?>
