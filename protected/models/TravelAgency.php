<?php

/**
 * This is the model class for table "tbl_travel_agency".
 *
 * The followings are the available columns in table 'tbl_travel_agency':
 * @property integer $id
 * @property string $name
 * @property string $telephone
 * @property string $mobile_phone
 * @property string $email
 * @property string $street
 * @property string $postal_code
 * @property string $city
 * @property integer $country_id
 * @property string $create_time
 * @property string $update_time
 *
 *
 * The followings are the available model relations:
 * @property Client[] $clients
 */
class TravelAgency extends TourManagerActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @return TravelAgency the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_travel_agency';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'unique'),
			array('name, city, country_id', 'length', 'max'=>64),
			array('telephone, mobile_phone', 'length', 'max'=>32),
			array('email, street', 'length', 'max'=>256),
			array('postal_code', 'length', 'max'=>8),
			array('email', 'email'),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, telephone, mobile_phone, email, street, postal_code, city, country_id, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clients' => array(self::HAS_MANY, 'Client', 'agency_id'),
			'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'telephone' => 'Telephone',
			'mobile_phone' => 'Mobile Phone',
			'email' => 'Email',
			'street' => 'Street',
			'postal_code' => 'Postal Code',
			'city' => 'City',
			'country_id' => 'Country',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('mobile_phone',$this->mobile_phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('postal_code',$this->postal_code,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('country_id',$this->country_id,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);


		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return string the counntry name related to this travel agency
	 */
	public function getCountryName()
	{
		if ($this->country)
			return $this->country->name;

		else return 'Not Set';
	}
}