<?php

/**
 * This is the model class for table "tbl_hotel".
 *
 * The followings are the available columns in table 'tbl_hotel':
 * @property integer $id
 * @property string $name
 * @property string $telephone
 * @property string $email
 * @property string $street
 * @property string $postal_code
 * @property string $city
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property Client[] $clients
 */
class Hotel extends TourManagerActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Hotel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_hotel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'unique'),
			array('name, city', 'length', 'max'=>64),
			array('telephone', 'length', 'max'=>32),
			array('email, street', 'length', 'max'=>256),
			array('email', 'email'),
			array('postal_code', 'length', 'max'=>8),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, telephone, email, street, postal_code, city, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clients' => array(self::HAS_MANY, 'Client', 'hotel_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'telephone' => 'Telephone',
			'email' => 'Email',
			'street' => 'Street',
			'postal_code' => 'Postal Code',
			'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('postal_code',$this->postal_code,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

}