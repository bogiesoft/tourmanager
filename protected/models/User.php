<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $name
 * @property string $last_login_time
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 *
 * The followings are the available model relations:
 * @property Client[] $clients
 */
class User extends TourManagerActiveRecord
{
	public $password_repeat;

	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, password_repeat', 'required'),
			array('username, password, email, name', 'length', 'max'=>256),
			array('username', 'length', 'min'=>4),
			// Only numbers and letters and underscore
			array('username', 'match', 'pattern'=>'/^([a-zA-Z0-9_])+$/',
				'message'=>'Only numbers, letters and underscore allowed in username'),
			array('password', 'length', 'min'=>8),
			array('password', 'strongPassword'),
			array('email, username', 'unique'),
			array('email', 'email'),
			array('password', 'compare'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, email, name, create_time, update_time, create_user_id, update_user_id', 'safe', 'on'=>'search'),
			// Tell UserControler to set this attribute with values in the create form
			array('password_repeat', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clients' => array(self::HAS_MANY, 'Client', 'create_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'name' => 'Name',
			'last_login_time' => 'Last Login Time',
			'create_time' => 'Create Time',
			'create_user_id' => 'Create User',
			'update_time' => 'Update Time',
			'update_user_id' => 'Update User',
			'password_repeat' => 'Repeat Password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('update_user_id',$this->update_user_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));

	}

	/**
	 *perform one-way encryption on the password before we store it in the database
	 */
	public function afterValidate()
	{
	  parent::afterValidate();
	  $this->password = $this->encrypt($this->password);
	}

	public function encrypt($value)
	{
	  return md5($value);
	}

	/**
	 * Ensures passwords have at least letters and numbers
	 */
	public function strongPassword()
	{
	  if (preg_match('/^([a-zA-Z])+$/', $this->password))
	  	if (preg_match('/^([0-9])+$/', $this->password))
	  		return;

		else
		{
		  $this->addError('password','Password must have letters and numbers.');
		}
	}

}