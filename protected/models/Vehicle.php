<?php

/**
 * This is the model class for table "tbl_vehicle".
 *
 * The followings are the available columns in table 'tbl_vehicle':
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $license
 * @property integer $capacity
 * @property string $create_time
 * @property string $update_time
 *
 * The followings are the available model relations:
 * @property Activity[] $activities
 */
class Vehicle extends TourManagerActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Vehicle the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_vehicle';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, capacity, name', 'required'),
			array('capacity', 'numerical', 'integerOnly'=>true),
			array('name, type', 'length', 'max'=>64),
			array('license', 'length', 'max'=>32),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, type, license, capacity, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activities'=>array(self::MANY_MANY, 'Activity', 
				'rel_activity_vehicle(activity_id, vehicle_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'type' => 'Type',
			'license' => 'License',
			'capacity' => 'Capacity',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('license',$this->license,true);
		$criteria->compare('capacity',$this->capacity);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	  * Associates a vehicle with Activity by populating rel_activity_vehicle table
	  */
	 public function associateVehicleWithActivity($activityId)
	 {
	 	$sql = "INSERT INTO rel_activity_vehicle (activity_id, vehicle_id) " .
	 			"VALUES (:activityId, :vehicleId)";
	 	
	 	$command = Yii::app()->db->createCommand($sql);
	 	$command->bindValue(":activityId", $activityId, PDO::PARAM_INT);
	 	$command->bindValue(":vehicleId", $this->id, PDO::PARAM_INT);
	 	
	 	// Catch exception when inserting existing relation
	 	try
	 	{
	 		$command->execute();
	 	} catch(CDbException $e) 
	 	{
	 		return false;	
	 	}
	 	
	 	return true;
	 }
	 
	 /**
	  * Removes a relation of an Activity with a vehicle
	  * @param $activity the id of the activity to disassociate
	  * @return true if query is sucessfull, false otherwise
	  * 
	  */
	 public function disassociateVehicleWithActivity($activityId)
	 {
		 	$sql = "DELETE FROM rel_activity_vehicle WHERE activity_id = :activityId" .
		 			" AND vehicle_id = :vehicleId";
		 			
		 	$command = Yii::app()->db->createCommand($sql);
		 	$command->bindValue(":activityId", $activityId, PDO::PARAM_INT);
		 	$command->bindValue(":vehicleId", $this->id, PDO::PARAM_INT);
		 	
		 	// Catch exception when inserting deleting relation
		 	try
		 	{
		 		$command->execute();
		 	} catch(CDbException $e) 
		 	{
		 		return false;	
			 	}
			 	
			 	return true;
	 }
	
}