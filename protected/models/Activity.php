<?php

/**
 * This is the model class for table "tbl_activity".
 *
 * The followings are the available columns in table 'tbl_activity':
 * @property integer $id
 * @property string $date
 * @property string $time
 * @property integer $type_id
 * @property integer $completed
 * @property string $create_time
 * @property string $update_time
 * @property string $notes
 *
 * The followings are the available model relations:
 * @property ActivityType $type
 * @property Client[] $clients
 */
class Activity extends TourManagerActiveRecord
{
  //public $totalPax;

	/**
	 * Returns the static model of the specified AR class.
	 * @return Activity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_activity';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, time, type_id, completed', 'required'),
			array('date', 'date', 'format'=>'yyyy-MM-dd'),
			array('time', 'date', 'format'=>'h:m'),
			array('type_id, completed', 'numerical', 'integerOnly'=>true),
			array('create_time, update_time, client, notes', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date, time, completed, type_id, notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'type' => array(self::BELONGS_TO, 'ActivityType', 'type_id'),
			'clients' => array(self::HAS_MANY, 'Client', 'activity_id'),
			'vehicles' => array(self::MANY_MANY, 'Vehicle',
				'rel_activity_vehicle(activity_id, vehicle_id)'),
			'staff_members'=>array(self::MANY_MANY, 'StaffMember',
				'rel_activity_staff(activity_id, staff_member_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'time' => 'Time',
			'type_id' => 'Type',
			'completed' => 'Completed',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'name' => 'Activity',
			'vehicle_list' => 'List of Vehicles',
			'staff_list' => 'Staff',
			'activity_type_name'=>'Activity Type',
			'totalCashDirect' => 'Total Cash Direct €',
			'totalCashCredit' => 'Total Cash Credit €',
			'totalCash' => 'Total Cash €',
			'completedValue'=>'Completed',
			'notes'=>'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		// We need to convert ActivityType id's to strings before comparing
/*
		$instance = new ActivityType();
		if(isset($this->type_id))
		{
			$instance = ActivityType::model()->findByAttributes(
				array('name'=>$this->type_id));

			// If no record was found
			if ($instance == NULL)
				$instance->id = "";
		}
*/

		$criteria->compare('date', $this->date);

		$criteria->compare('id', $this->id);

		$criteria->compare('time',$this->time);
		$criteria->compare('type_id', $this->type_id);

		$criteria->compare('completed',$this->completed);
		$criteria->compare('notes', $this->notes, true);
		
		$criteria->order = 'date';

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Calculates the number of available seats for the activity. The number of
	 * available seats is the total number of seats in all vehicles associated to
	 * the activity, less the number of pax of all clients (reservations)
	 * associated with this activity
	 * @return number of available seats for the activity
	 */
	public function getAvailableSeats()
	{
		return $this->totalSeats - $this->totalPax;
	}

	/**
	 * Calculates the number of pax by summing total number of pax for each
	 * Client (reservation) associated with this Activity
	 * @return the total number of pax in the Activity
	 */
	public function getTotalPax()
	{
		return $this->totalAdults + $this->totalChildren;
	}


	/**
	 * Calculates the number of adults by summing total number of adults for each
	 * Client (reservation) associated with this Activity
	 * @return the total number of adults in the Activity
	 */
	public function getTotalAdults()
	{

		$totalAdults = 0;

		foreach($this->clients as $client)
			$totalAdults += $client->pax;

		return $totalAdults;

	}

	/**
	 * Calculates the number of children by summing total number of children
	 * for each Client (reservation) associated with this Activity
	 * @return the total number of children in the Activity
	 */
	public function getTotalChildren()
	{
		$totalChildren = 0;

		foreach($this->clients as $client)
			$totalChildren += $client->number_children;

		return $totalChildren;
	}

	/**
	 * Calculates the number of seats by summing the capacity of
	 * each vehice associated with this Activity
	 * @return the total number of seats in the Activity
	 */
	public function getTotalSeats()
	{
		$totalSeats = 0;

		foreach($this->vehicles as $vehicle)
			$totalSeats += $vehicle->capacity;

		return $totalSeats;
	}

	/**
	 * Creates a client associated to this Activity
	 * @return true is client is created, false otherwise
	 */
	public function addClient($client)
	{
		$client->activity_id = $this->id;
	 	return $client->save();
	}

	/**
	 * Makes changes to date and time format and to convert from human readable
	 * strings to ids
	 */
	protected function beforeValidate()
	{
		// Convert ActivityType name to it's id
//		$activityType = ActivityType::model()->findByAttributes(
//			array('name'=>$this->type_id));

		// Only replace name by id if the ActivityType exists
//		if (count($activityType) != 0)
//			$this->type_id = $activityType->id;

		// Replace strings by integers
		if ($this->completed == 'Yes')
			$this->completed = 1;

		else if ($this->completed == 'No')
			$this->completed = 0;

		return parent::beforeValidate();
	}

	/**
	 * Makes changes to date and time format
	 */
	protected function afterFind()
	{
		parent::afterFind();

		$this->time = substr($this->time, 0, 5); // Cut the :00 from time

	}

	public function getActivityType()
	{
		return $this->type->name;
	}

	public function getName()
	{
		return '#'. $this->id ." - " . $this->type->name." in ".$this->date." - ".
			$this->time;
	}

	public function getNameAndAvailableSeats()
	{
		return $this->type->name." | Available Seats: ".$this->getAvailableSeats();
	}

	/**
	 * Overrides parent getAllNames() function
	 */
	public function getAllNames($useIds = false)
	{
		return null;
	}

	 /**
	  * Clear all Vehicles relations with this Activity
	  * @return true if query is sucessfull, false otherwise
	  */
	protected function clearVehicles()
	{
	 	$sql = "DELETE FROM rel_activity_vehicle WHERE activity_id = :activityId";

	 	$command = Yii::app()->db->createCommand($sql);
	 	$command->bindValue(":activityId", $this->id, PDO::PARAM_INT);

	 	// Catch exception when inserting existing relation
	 	try
	 	{
	 		$command->execute();
	 	} catch(CDbException $e)
	 	{
	 		return false;
	 	}

	 	return true;
	 }

	  /**
	  * Clear all StaffMember relations with this Activity
	  * @return true if query is sucessfull, false otherwise
	  */
	 protected function clearStaffMembers()
	 {
	 	$sql = "DELETE FROM rel_activity_staff WHERE activity_id = :activityId";

	 	$command = Yii::app()->db->createCommand($sql);
	 	$command->bindValue(":activityId", $this->id, PDO::PARAM_INT);

	 	// Catch exception when inserting existing relation
	 	try
	 	{
	 		$command->execute();
	 	} catch(CDbException $e)
	 	{
	 		return false;
	 	}

	 	return true;
	 }

   /**
	 * Creates a data provider of clients related to this activity
   * @param name of the ActiviRecord to retrieve the data provider
	 * @param int $id of the activity from where to retrieve related objects
	 * @return CActiveDataProvider holding related clients
	 */
	public function getRelatedData($model)
	{
		// Create dataProvider
		$dataProvider = new CActiveDataProvider($model, array(
			'criteria'=>array(
				'with'=>array('activities'=>array(
					'condition'=>'activity_id='.$this->id,
					'together'=>true, //Selects only related activity record
					),
				),
			),
			'pagination'=>array(
				'pagesize'=>10,
			),
		));

		return $dataProvider;
	}

  /**
   * Creates a data provider of active records unrelated to this activity
	 * The equivalied of a NOT IN SQL clause
   * @param string name of the ActiviRecord to retrieve the data provider
   * @param boolean if data related to old activities should by excluded
	 * @return CActiveDataProvider A data provider of unrelated objects
	 */
	public function getUnrelatedData($model, $excludeOld = false)
	{

    $dataProvider = $this->getRelatedData($model);

		// Get unrelated data
		$exclude = array(); // Array with excluded ids
		foreach ($dataProvider->data as $data)
			$exclude[] = $data->id;

    // If we should exclude objects related to old activities
    if ($excludeOld == true)
    {
      // Get data recently related and add it to exclude array
      $oldModels = $this->getOldData($model);
      foreach ($oldModels->data as $data)
        $exclude[] = $data->id;
    }

		$notInRelationCriteria = new CDbCriteria;

    //Exclude related models
		$notInRelationCriteria->addNotInCondition('id', $exclude);


		$unrelatedDataProvider = new CActiveDataProvider($model, array(
			'criteria'=>$notInRelationCriteria,
			'pagination'=>array(
				'pagesize'=>10,
				),
			)
		);
		return $unrelatedDataProvider;
	}

  /**
   * Retrives a data related to some Actvity more then 1 month old
   * @param string ActiveRecord name
   * @return CActiveDataProvider related to some activity older then 1 month ago
   */
  public function getOldData($model)
  {
    // Get 4 weeks ago
    $lastMonth = date('Y-m-d', strtotime('-4 week'));

    // Get way way back, 100 years should be enough
    $wayBack = date('Y-m-d', strtotime('-100 year'));

    	// Create dataProvider
		$dataProvider = new CActiveDataProvider($model, array(
			'criteria'=>array(
				'with'=>array('activities'=>array(
					'condition'=> "date BETWEEN '".$wayBack."' AND '".$lastMonth."'",
					'together'=>true, //Selects only related activity record
					),
				),
			),
			'pagination'=>array(
				'pagesize'=>10,
			),
		));

		return $dataProvider;
  }

  public static function getRecentActivities($days = 7)
  {

  	$criteria = new CDbCriteria;

	  $today = date('Y-m-d');

  	// Check if parameter type and if it fails use the default 7 days interval
  	if (is_int($days))
	  	$future = date('Y-m-d', strtotime("+$days day"));

  	else $future = date('Y-m-d', strtotime("+7 day"));

  	$criteria->addBetweenCondition('date', $today, $future);

  	$dataProvider = new CActiveDataProvider('Activity', array(
  		'criteria'=>$criteria
  	));

  	return $dataProvider;

  }

  /**
   * Calculates the total direct cash payed for this activity
   * @return float total direct cash for this activity
   */
  public function getTotalCashDirect()
  {
  	$totalCash = 0.00;

  	if (isset($this->clients))
  	{
  		foreach($this->clients as $client)
  			$totalCash += $client->getTotalDirect();
  	}

  	return $totalCash;
  }

  /**
   * Calculates the total credit cash payed for this activity
   * @return float total credit cash for this activity
   */
  public function getTotalCashCredit()
  {
	$totalCash = 0.00;

  	if (isset($this->clients))
  	{
  		foreach($this->clients as $client)
  			$totalCash += $client->getTotalCredit();
  	}

  	return $totalCash;
  }

  /**
   * @return float total cash payed for this activity
   */
  public function getTotalCash()
  {
    return $this->getTotalCashCredit() + $this->getTotalCashDirect();
  }

  /**
   * @return the compled string value: 'Yes' or 'No'
   */
  public function getCompletedValue()
  {
  	if ($this->completed == 1)
  		return 'Yes';
  	else return 'No';
  }
}
