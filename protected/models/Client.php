<?php

/**
 * This is the model class for table "tbl_client".
 *
 * The followings are the available columns in table 'tbl_client':
 * @property integer $id
 * @property string $name
 * @property string $telephone
 * @property string $email
 * @property integer $hotel_id
 * @property integer $room_number
 * @property integer $pax
 * @property integer $activity_id
 * @property float $value_payed_adult
 * @property float $value_payed_children
 * @property float $value_prepayed
 * @property string $notes
 * @property string $create_time
 * @property string $update_time
 * @property string $country_id
 * @property integer $number_children
 * @property integer $create_user_id
 * @property integer $paymment_type
 * @property string voucher
 * @property integer arrived
 *
 * The followings are the available model relations:
 * @property Activity $activities
 * @property TravelAgency $agency
 * @property Hotel $hotel
 * @property User $user
 */
class Client extends TourManagerActiveRecord
{
	private $_arrived_options;
	/**
	 * @var paymment type: direct paymment
	 */
	const DIRECT = 0;

	/**
	 * @var paymment type: credit paymment
	 */
	const CREDIT = 1;

	/**
	 * Returns the static model of the specified AR class.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, pax', 'required'),
			array('hotel_id, room_number, agency_id, pax, number_children, country_id, paymment_type, activity_id', 'numerical', 'integerOnly'=>true),
			array('hotel_id, room_number, agency_id, pax, number_children', 'numerical', 'min'=>0),
			array('value_payed_adult, value_payed_children, value_prepayed', 'numerical', 'min'=>0.0),
			array('name, voucher', 'length', 'max'=>64),
			array('telephone', 'length', 'max'=>32),
			array('email', 'length', 'max'=>256),
			array('arrived', 'length', 'max'=>6),
			array('email', 'email'),
			array('notes, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, telephone, email, hotel_id, agency_id, pax, value_payed, notes, country_id, number_children, voucher, arrived', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'agency' => array(self::BELONGS_TO, 'TravelAgency', 'agency_id'),
			'hotel' => array(self::BELONGS_TO, 'Hotel', 'hotel_id'),
			'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
			'user'=>array(self::BELONGS_TO, 'User', 'create_user_id'),
			'activity'=>array(self::BELONGS_TO, 'Activity', 'activity_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'telephone' => 'Phone',
			'email' => 'Email',
			'hotel_id' => 'Hotel',
			'room_number' => 'Room #',
			'agency_id' => 'Agency',
			'pax' => 'Adults',
			'value_payed_children' => 'Value Payed (Children)',
			'value_payed_adult' => 'Value Payed (Adults)',
			'notes' => 'Notes',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'country_id' => 'Country',
			'number_children' => 'Children',
			'value_prepaid' => 'Prepaid value',
			'hotelName' => 'Hotel',
			'travelAgencyName' => 'Travel Agency',
			'countryName' => 'Country',
			'paymment_type' => 'Paymment Type',
			'voucher' => 'Voucher',
			'totalDirect' => 'Total Direct',
			'totalDirectOrVoucher' => '€/V',
			'totalCredit' => 'Total Credit',
			'debt' => 'Debt',
			'arrived' => 'Arrived',
			'arrivedStatus' => 'Arrived'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @param $recent true if the method should only search recent clients
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($recent = false)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;


		// Search agency by name
		$agency = TravelAgency::model()->findByAttributes(array('name'=>$this->agency_id));
		if(isset($agency))
		{
	  		$criteria->compare('agency_id', $agency->id, true);
		}
		else $criteria->compare('agency_id', $this->agency_id, true);

		// Search Hotel by name
		$hotel = Hotel::model()->findByAttributes(array('name'=>$this->hotel_id));
		if(isset($hotel))
		{
		  $criteria->compare('hotel_id',$hotel->id, true);
		}
		else $criteria->compare('hotel_id', $this->hotel_id, true);
		
		$country = Country::model()->findByAttributes(array('name'=>$this->country_id));
		if(isset($country))
		{
			$criteria->compare('country_id', $country->id, true);
		}
		else $criteria->compare('country_id', $this->country_id, true);

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('room_number',$this->room_number);
		$criteria->compare('pax',$this->pax);
		$criteria->compare('value_payed_adult',$this->value_payed_adult);
		$criteria->compare('value_payed_children',$this->value_payed_children);
		$criteria->compare('value_prepayed',$this->value_prepayed);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('number_children',$this->number_children, true);
		$criteria->compare('paymment_type',$this->paymment_type, true);
		$criteria->compare('voucher',$this->voucher, true);
		$criteria->compare('arrived',$this->arrived, true);

		// Search only recent clients so that they are shown in main activity view
		// This causes less overhead
		if ($recent === true)
		{
			$lastTwoWeeks = date('Y-m-d', strtotime('-2 weeks'));

			// +1 day includes today's clients
   		$today = date('Y-m-d', strtotime("+1 day"));
			$criteria->addBetweenCondition('create_time', $lastTwoWeeks, $today);
		
		}

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

	public function getTravelAgencyName()
	{
		if (isset($this->agency))
			return $this->agency->name;

		return 'Not set';
	}

	public function getHotelName()
	{
		if (isset($this->hotel))
			return $this->hotel->name;

		return 'Not set';
	}

	public function getCountryName()
	{
		if (isset($this->country))
			return $this->country->name;

		return 'Not Set';
	}

  /**
   * Replaces Agencies and Hotel names by their id's before validating forms
   */
  public function beforeValidate()
  {
    // Replace strings with primary keys for hotels and agencies
    if (isset($this->agency_id))
    {
    	$agency = TravelAgency::model()->findByAttributes(array('name'=> $this->agency_id));
    	if (isset($agency))
    		$this->agency_id = $agency->id;
    }

	  if (isset($this->hotel_id))
	  {
	  	$hotel = Hotel::model()->findByAttributes(array('name'=> $this->hotel_id));
	  	if (isset($hotel))
	  		$this->hotel_id = $hotel->id;
	  }

    return parent::beforeValidate();
  }


	/**
	 * @return array with payment types (integer) and their descriptions
	 */
	public function getPaymmentTypes()
	{
		return array(
			self::DIRECT => 'Direct',
			self::CREDIT => 'Credit',
		);

	}

	/**
	 * @return string paymment type name
	 */
	public function getPaymmentTypeName()
	{
	  $paymmentTypes = $this->getPaymmentTypes();
	  return isset($paymmentTypes[$this->paymment_type]) ? $paymmentTypes[$this->paymment_type] :
	    "unknown paymment type ({$this->paymment_type})" ;
	}

	/**
	 * Calculate the total price payed with this client reservation direct
	 * @return float total price payed with this client reservation
	 */
	public function getTotalDirect()
	{
		$total = 0.00;
		$totalAdults = 0;
		$totalChildren = 0;

		if (isset($this->pax))
			$totalAdults = $this->pax;

		if (isset($this->number_children))
			$totalChildren = $this->number_children;

		// Check paymment type and calculate the total value payed
		if ($this->paymment_type == self::DIRECT)
		{
			$total = ($totalAdults * $this->value_payed_adult) +
				($totalChildren * $this->value_payed_children);
		}

		return $total;
	}

	/**
	 * Calculate the total price payed with this client reservation credit
	 * @return float total price payed with this client reservation
	 */
	public function getTotalCredit()
	{
		$total = 0.00;
		$totalAdults = 0;
		$totalChildren = 0;

		if (isset($this->pax))
			$totalAdults = $this->pax;

		if (isset($this->number_children))
			$totalChildren = $this->number_children;

		if($this->paymment_type == self::CREDIT)
		{
			$total = ($totalAdults * $this->value_payed_adult) +
				($totalChildren * $this->value_payed_adult);
		}

		return $total;
	}

	/**
	 * @return string The value payed by the client if we are in
	 * presence of a direct sale or a the voucher number if a credit sale
	 */
	public function getTotalDirectOrVoucher()
	{
		$total = $this->getTotalDirect();

		if ($total == 0)
			return $this->voucher;

		else return '€ '.(string)$total;
	}

	/**
	 * @return float the value the client has in debt
	 */
	public function getDebt()
	{
		if ($this->paymment_type == self::DIRECT)
			return $this->getTotalDirect() - $this->value_prepayed;

		else return 0.00;
	}

	/**
	 * @return array with yes or no for arrived field
	 */
	public function getArrivedOptions()
	{
		if (!isset($this->_arrived_options))
		{
			$this->_arrived_options = array('No'=>'No','Yes'=>'Yes');
		}

		return $this->_arrived_options;
	}

}
