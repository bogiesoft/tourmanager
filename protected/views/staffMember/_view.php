<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('freelancerStatus')); ?>:</b>
	<?php echo CHtml::encode($data->freelancerStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roleName')); ?>:</b>
	<?php echo CHtml::encode($data->roleName); ?>
	<br />


</div>