<?php
$this->breadcrumbs=array(
	'Staff Members',
);

$this->menu=array(
	array('label'=>'Create Staff Member', 'url'=>array('create')),
	array('label'=>'Manage Staff Member', 'url'=>array('admin')),
);
?>

<h1>Staff Members</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
