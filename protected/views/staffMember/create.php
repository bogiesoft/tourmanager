<?php
$this->breadcrumbs=array(
	'Staff Members'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Staff Member', 'url'=>array('index')),
	array('label'=>'Manage Staff Member', 'url'=>array('admin')),
);
?>

<h1>Create Staff Member</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,
											   'roles'=>$roles)); ?>