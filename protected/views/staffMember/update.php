<?php
$this->breadcrumbs=array(
	'Staff Members'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Staff Member', 'url'=>array('index')),
	array('label'=>'Create Staff Member', 'url'=>array('create')),
	array('label'=>'View Staff Member', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Staff Member', 'url'=>array('admin')),
);
?>

<h1>Update Staff Member <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,
												'roles'=>$roles,
												)); ?>