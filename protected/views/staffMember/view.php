<?php
$this->breadcrumbs=array(
	'Staff Members'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Staff Member', 'url'=>array('index')),
	array('label'=>'Create Staff Member', 'url'=>array('create')),
	array('label'=>'Update Staff Member', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Staff Member', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Staff Member', 'url'=>array('admin')),
);
?>

<h1>View Staff Member #<?php echo $model->id; ?></h1>

<?php $this->widget('TDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'freelancerStatus',
		'roleName',
	),
)); ?>
