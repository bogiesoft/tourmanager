<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'client-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pax'); ?>
		<?php echo $form->textField($model,'pax', array('size'=>3, 'maxlenght'=>4)); ?>
		<?php echo $form->error($model,'pax'); ?>
		<span class="hint"><?php echo "Only adults should be introduced here"; ?></span>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'number_children'); ?>
		<?php echo $form->textField($model,'number_children', array('size'=>3, 'maxlenght'=>4)); ?>
		<?php echo $form->error($model,'number_children'); ?>
	</div>


<?php
	// This form is used by activity as well, so we need to give absolute paths
	// within the app so the view file can be resolved
	$this->widget('zii.widgets.jui.CJuiAccordion', array(
    'panels'=>array(
        'Contacts and Nationality'=>$this->renderPartial('/client/_contacts',
        	array(
				'model'=>$model,
				'form'=>$form,
				'hotels'=>$hotels,
				'agencies'=>$agencies,
			),
		true),
        'Payments and Notes'=>$this->renderPartial('/client/_payments',
        array(
			'model'=>$model,
			'form'=>$form,
			),
		true),

    ),
    // additional javascript options for the accordion plugin
    'options'=>array(
        //'animated'=>'bounceslide',
        'collapsible'=>true,
    ),
));

?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->