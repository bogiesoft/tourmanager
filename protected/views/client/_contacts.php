
<div class="row">
	<?php echo $form->labelEx($model,'telephone'); ?>
	<?php echo $form->textField($model,'telephone',array('size'=>32,'maxlength'=>32)); ?>
	<?php echo $form->error($model,'telephone'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'email'); ?>
	<?php echo $form->textField($model,'email',array('size'=>32,'maxlength'=>256)); ?>
	<?php echo $form->error($model,'email'); ?>
</div>

<div class="row">
		<?php echo $form->labelEx($model,'hotel_id'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
	      	'source'=>$hotels,
	      	'model'=>$model,
	      	'attribute'=>'hotel_id',
	    	));
		?>
		<?php echo $form->error($model,'hotel_id'); ?>
</div>

<div class="row">
		<?php echo $form->labelEx($model,'room_number'); ?>
		<?php echo $form->textField($model,'room_number', array('size'=>3, 'maxlenght'=>4)); ?>
		<?php echo $form->error($model,'room_number'); ?>
	</div>

<div class="row">
		<?php echo $form->labelEx($model,'agency_id'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
	      	'source'=>$agencies,
	      	'model'=>$model,
	      	'attribute'=>'agency_id',
	    	));
		?>
		<?php echo $form->error($model,'agency_id'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'country_id'); ?>
	<?php echo $form->dropDownList($model, 'country_id', CHtml::listdata(Country::model()->findAll(), 'id', 'name'), array('options' => array('122'=>array('selected'=>true)))); ?>
  	<?php echo $form->error($model,'country_id'); ?>
</div>

