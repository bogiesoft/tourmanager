<?php
$this->breadcrumbs=array(
	'Clients'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Client', 'url'=>array('index')),
	array('label'=>'Update Client', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Client', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Client', 'url'=>array('admin')),
);
?>

<h1>View Client #<?php echo $model->id; ?></h1>

<?php $this->widget('TDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'telephone',
		'email',
		array(
			'name'=>'hotel_id',
			'value'=>CHtml::encode($model->getHotelName())
		),
		'room_number',
		array(
			'name'=>'travel_agency',
			'value'=>CHtml::encode($model->getTravelAgencyName())
		),
		'voucher',
		'pax',
		'number_children',
		'value_payed_adult',
		'value_payed_children',
		'value_prepayed',
		'notes',
		array(
			'name'=>'country_id',
			'value'=>CHtml::encode($model->getCountryName())
		),
		array(
			'name'=>'paymment_type',
			'value'=>CHtml::encode($model->getPaymmentTypeName())
		),
		array(
			'name'=>'Total Direct',
			'value'=>CHtml::encode($model->getTotalDirect()),
		),
		array(
			'name'=>'Total Credit',
			'value'=>CHtml::encode($model->getTotalCredit()),
		),

		array(
			'name'=>'Debt',
			'value'=>$model->getDebt(),
		),
		'arrived'
	)
)); ?>
