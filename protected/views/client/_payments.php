<div class="row">
		<?php echo $form->labelEx($model,'paymment_type'); ?>
		<?php echo $form->dropDownList($model, 'paymment_type', $model->getPaymmentTypes()); ?>
		<?php echo $form->error($model,'paymment_type'); ?>
</div>

<div class="row">
		<?php echo $form->labelEx($model,'voucher'); ?>
		<?php echo $form->textField($model, 'voucher', array('size'=>32, 'maxlength'=>64)); ?>
		<?php echo $form->error($model,'voucher'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'value_payed_adult'); ?>
	<?php echo $form->textField($model,'value_payed_adult', array('size'=>3, 'maxlenght'=>4)); ?>
	<?php echo $form->error($model,'value_payed_adult'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'value_payed_children'); ?>
	<?php echo $form->textField($model,'value_payed_children', array('size'=>3, 'maxlenght'=>4)); ?>
	<?php echo $form->error($model,'value_payed_children'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'value_prepayed'); ?>
	<?php echo $form->textField($model,'value_prepayed', array('size'=>3, 'maxlenght'=>4)); ?>
	<?php echo $form->error($model,'value_prepayed'); ?>
</div>

<div class="row">
	<p id="total_cash"></p>
	<script type="text/javascript">
	getTotalPayed();
	</script>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'arrived'); ?>
	<?php echo $form->dropDownList($model,'arrived', $model->arrivedOptions); ?>
	<?php echo $form->error($model,'arrived'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'notes'); ?>
	<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
	<?php echo $form->error($model,'notes'); ?>
</div>
