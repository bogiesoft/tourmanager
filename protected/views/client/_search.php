<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id', array('size'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>64)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>32,'maxlength'=>256)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pax'); ?>
		<?php echo $form->textField($model,'pax', array('size'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'number_children'); ?>
		<?php echo $form->textField($model,'number_children', array('size')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'value_payed_adult'); ?>
		<?php echo $form->checkBox($model,'value_payed_adult'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'value_payed_children'); ?>
		<?php echo $form->checkBox($model,'value_payed_children'); ?>
	</div>


	<div class="row">
		<?php echo $form->label($model,'value_prepayed'); ?>
		<?php echo $form->checkBox($model,'value_prepayed'); ?>
	</div>
	<div class="row">
		<?php echo $form->label($model,'hotel_id'); ?>
		<?php echo $form->textField($model,'hotel_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'room_number'); ?>
		<?php echo $form->textField($model,'room_number', array('size'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agency_id'); ?>
		<?php echo $form->textField($model,'agency_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'country_id'); ?>
		<?php echo $form->textField($model,'country_id'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
