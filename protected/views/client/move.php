<?php
$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Move',
);

$this->menu=array(
	array('label'=>'List Client', 'url'=>array('index')),
	array('label'=>'Manage Client', 'url'=>array('admin')),
);
?>

<div class="info">
<h3><?php echo Yii::t('UI', 'Select a Activity from the list below to copy/move' .
		' "clientName" to ', array('clientName'=>$model->name)); ?></h3>

</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'move-client-form',
	'enableAjaxValidation'=>false,
)); ?>

<div class="activity-grid">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'activity-client-grid',
		'dataProvider'=>$activities,
		'columns'=>array(
			'id',
			array( // display 'Activity.type_id' using an expression
            'name'=>'type_id',
            'value'=>'$data->type->name',
        ),
			'date',
			'time',
			
      
      array( // Display a column with available seats
      	'name'=> Yii::t('UI', 'Availabe Seats'),
      	'value'=>'$data->getAvailableSeats()',
      ),
      
			array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{add}{move}',
			
			'viewButtonUrl'=>'Yii::app()->createUrl("/client/view", array("id"=>$data["id"]))',
			'viewButtonLabel'=> Yii::t('ToolTips', 'View Client'),
			
			// Move button
			'buttons'=> array(
				'add'=>array(
						'label'=>Yii::t('ToolTips', 'Copy to this Activiy'),
						'imageUrl'=> Yii::app()->baseUrl.'/images/list-add.png',
						'url'=>'Yii::app()->createUrl(' .
								'"/client/copyToActivity", ' .
								'array(' .
									'"id"=>'."$model->id".', ' .
									'"newActivityId"=>$data["id"], ' .
									'"activityId"=>'."$activityId".'))',
					),
				'move'=>array(
						'label'=>Yii::t('ToolTips', ' Move to this Actvivity'),
						'imageUrl'=> Yii::app()->baseUrl.'/images/object-rotate-right.png',
						'url'=>'Yii::app()->createUrl(' .
								'"/client/moveToActivity", ' .
								'array("id"=>'."$model->id".', ' .
								'"newActivityId"=>$data["id"],' .
								'"activityId"=>'."$activityId".'))',
					)
			),
		),
	))); ?>
</div>

<?php $this->endWidget(); ?>
