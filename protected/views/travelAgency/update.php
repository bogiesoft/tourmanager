<?php
$this->breadcrumbs=array(
	'Travel Agencys'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TravelAgency', 'url'=>array('index')),
	array('label'=>'Create TravelAgency', 'url'=>array('create')),
	array('label'=>'View TravelAgency', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TravelAgency', 'url'=>array('admin')),
);
?>

<h1>Update TravelAgency <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>