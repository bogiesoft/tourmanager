<?php
$this->breadcrumbs=array(
	'Travel Agencys'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TravelAgency', 'url'=>array('index')),
	array('label'=>'Manage TravelAgency', 'url'=>array('admin')),
);
?>

<h1>Create Travel Agency</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>