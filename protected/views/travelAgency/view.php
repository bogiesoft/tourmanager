<?php
$this->breadcrumbs=array(
	'Travel Agencys'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TravelAgency', 'url'=>array('index')),
	array('label'=>'Create TravelAgency', 'url'=>array('create')),
	array('label'=>'Update TravelAgency', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TravelAgency', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TravelAgency', 'url'=>array('admin')),
);
?>

<h1>View Travel Agency <?php echo $model->name; ?></h1>

<?php $this->widget('TDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'telephone',
		'mobile_phone',
		'email',
		'street',
		'postal_code',
		'city',
		'countryName',
	),
)); ?>
