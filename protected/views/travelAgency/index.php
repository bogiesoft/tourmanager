<?php
$this->breadcrumbs=array(
	'Travel Agencys',
);

$this->menu=array(
	array('label'=>'Create TravelAgency', 'url'=>array('create')),
	array('label'=>'Manage TravelAgency', 'url'=>array('admin')),
);
?>

<h1>Travel Agencys</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
