<?php $this->widget('TDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'date',
		'time',
		array( // display 'Activity.type_id' using an expression
            'name'=>'type_id',
            'value'=> $model->type->name,
        ),
		'completedValue',
		'notes',
	),
)); ?>
