<?php
$this->breadcrumbs=array(
	'Activities'=>array('/activity'),
	'Calendar',
);

$this->menu=array(
	array('label'=>'Create Activity', 'url'=>array('create')),
	array('label'=>'Manage Activity', 'url'=>array('admin')),
);
?>

<h2 align="center"><?php echo $calendar->title; ?></h2>
<?php echo $calendar->printStartForm(); ?>
<?php echo $calendar->storePreviousLink; ?>
  .::
<?php echo $calendar->printControlMenu();?>
::.
<?php echo $calendar->storeNextLink; ?>

<?php echo $calendar->printCloseForm(); ?>
<?php echo $calendar->printCalendar(); ?>