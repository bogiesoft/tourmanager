<?php
$this->breadcrumbs=array(
	'Activities'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Activity', 'url'=>array('index')),
	array('label'=>'Create Activity', 'url'=>array('create')),
	array('label'=>'Update Activity', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Activity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Activity', 'url'=>array('admin')),
	array('label'=>'Mark as Completed', 'url'=>array('markAsCompleted', 'id'=>$model->id)),
);
?>

<h3><?php echo $model->name; ?></h3>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pax-activity-grid',
	'dataProvider'=>$model->search(),
	'summaryText'=> '',
	'columns'=>array(
		'availableSeats',
		'totalPax',
		'totalSeats',
	),
)); ?>


<?php if(Yii::app()->user->hasFlash('success')):?>
	<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success');?>
	</div>
<?php
	// Create fade effect
	Yii::app()->clientScript->registerScript(
	'fadeAndHideEffect',
	'$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");'
	);
endif; ?>

<?php if(Yii::app()->user->hasFlash('error')):?>
	<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error');?>
	</div>
<?php
	// Create fade effect
	Yii::app()->clientScript->registerScript(
	'fadeAndHideEffect',
	'$(".flash-error").animate({opacity: 1.0}, 5000).fadeOut("slow");'
	);
endif; ?>




<div id="activity-tabs">
<?php $this->widget('CTabView',
	array('tabs'=>array(

		'basic'=>array(
			'title'=>'Basic Information',
			'view'=>'_basic',
			'data'=>array('model'=>$model),
		),
		'clients'=>array(
			'title'=>'Clients',
			'view'=>'_client',
			'data'=>array(
				'clientDataProvider'=>$clientDataProvider,
				'client'=>$client,
				'model'=>$model,
				),

			),
		'vehicles'=>array(
			'title'=>'Vehicles',
			'view'=>'_vehicle',
			'data'=>array(
				'vehicleDataProvider'=>$vehicleDataProvider,
				'unrelatedVehicleDataProvider'=>$unrelatedVehicleDataProvider,
				'model'=>$model
				),
		),
		'staff'=>array(
			'title'=>'Staff',
			'view'=>'_staff',
			'data'=>array(
				'staffDataProvider'=>$staffDataProvider,
				'unrelatedStaffDataProvider'=>$unrelatedStaffDataProvider,
				'model'=>$model
			),

		),

		),
		'htmlOptions'=>array('style'=>"height: 1000px;"),
		'activeTab'=>$activeTab,
		'cssFile'=>Yii::app()->theme->baseUrl . '/css/ctabview.css',
		));
?>
</div>
