<?php
$this->breadcrumbs=array(
	'Activities'=>array('index'),
	'Calendar'=>array('calendar'),
	CHtml::encode($date),
);

$this->menu=array(
	array('label'=>'Create Activity', 'url'=>array('create')),
	array('label'=>'Manage Activity', 'url'=>array('admin')),
);
?>

<h1>Activities for <?php echo CHtml::encode($date) ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
