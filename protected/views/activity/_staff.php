<?php
Yii::app()->clientScript->registerScript('Staff', "
$('#add-staff-link').click(function(){
	$('#add-staff').toggle();
	return false;
});"
);
?>

<div id="related-staff">
<?php
// GridView with related staff
$this->widget('TGridView', array(
	'id'=>'busy-staff-grid',
    'dataProvider'=>$staffDataProvider,
    'columns'=>array(

    // Link column for vehicle names
  	array(
  		'class' =>'CLinkColumn',
  		'header'=> 'Name',
  		'labelExpression'=> '$data->name',
  		'urlExpression'=>'Yii::app()->createUrl("/staffMember/$data->id")'
		),

		//Other vehicle properties
		'roleName',
		'freelancerStatus',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
			'header'=>'Options',
			'viewButtonUrl'=>'Yii::app()->createUrl("/staffMember/view", ' .
					'array("id"=>$data["id"]))',

			'updateButtonUrl'=>'Yii::app()->createUrl("/staffMember/update", ' .
					'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',

			'deleteButtonLabel'=>'Remove from Activity',

			'deleteButtonUrl'=>'Yii::app()->createUrl(' .
					'"/staffMember/removeFromActivity", ' .
					'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',

      		'deleteButtonImageUrl'=>Yii::app()->baseUrl.'/images/list-remove.png',

			// Reload staff form to show the updated list
			'afterDelete'=>'afterStaffDelete',

			'deleteConfirmation'=> false,
		),
)));

?>
</div> <!--End related staff -->

<?php echo CHtml::link('+ Add Staff','#',array('id'=>'add-staff-link')); ?>
<div id="add-staff" style="display:none" >

<?php

// Gridview with unrelated staff
$this->widget('TGridView', array(
	'id'=>'available-staff-grid',
    'dataProvider'=>$unrelatedStaffDataProvider,
    'columns'=>array(

    	 // Link column for vehicle names
  	array(
  		'class' =>'CLinkColumn',
  		'header'=> 'Name',
  		'labelExpression'=> '$data->name',
  		'urlExpression'=>'Yii::app()->createUrl("/staffMember/$data->id")'
		),

			//Other vehicle properties
      'roleName',
      'freelancer',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{move}',
			'header'=>'Options',
			'viewButtonUrl'=>'Yii::app()->createUrl("/staffMember/view",' .
					'array("id"=>$data["id"]))',
			'updateButtonUrl'=>'Yii::app()->createUrl("/staffMember/update", ' .
					'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',

			// Move button
			'buttons'=> array(
				'move'=>array(
						'label'=>Yii::t('ToolTips', 'Add Staff to Activity'),
						'imageUrl'=> Yii::app()->baseUrl.'/images/list-add.png',
						'url'=>'Yii::app()->createUrl("/staffMember/addToActivity", ' .
								'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',
					)
			),
		),
)));
?>
</div><!-- add-staff-->



