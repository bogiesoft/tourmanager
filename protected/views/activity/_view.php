<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->name), array('/activity/view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('completed')); ?>:</b>
	<?php echo CHtml::encode($data->completed); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('Total PAX')); ?>:</b>
	<?php echo CHtml::encode($data->totalPax); ?>
	<br />


</div>
