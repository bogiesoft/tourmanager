<?php
Yii::app()->clientScript->registerScript('Vehicle', "
$('#add-vehicle-link').click(function(){
	$('#add-vehicle').toggle();
	return false;
});"
);
?>

<div id="related-vehicles">
<?php
$this->widget('TGridView', array(
	'id'=>'busy-vehicle-grid',
    'dataProvider'=>$vehicleDataProvider,
    'columns'=>array(

    // Link column for vehicle names
  	array(
  		'class' =>'CLinkColumn',
  		'header'=> 'Name',
  		'labelExpression'=> '$data->name',
  		'urlExpression'=>'Yii::app()->createUrl("/vehicle/$data->id")'
		),

		//Other vehicle properties
		'type',
		'capacity',

		// Actions
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
			'header'=>'Options',
			'viewButtonUrl'=>'Yii::app()->createUrl("/vehicle/view", ' .
					'array("id"=>$data["id"]))',

			'updateButtonUrl'=>'Yii::app()->createUrl("/vehicle/update", ' .
					'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',

			'deleteButtonLabel'=>'Remove from Activity',

			'deleteButtonUrl'=>'Yii::app()->createUrl(' .
					'"/vehicle/removeFromActivity", ' .
					'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',

      		'deleteButtonImageUrl'=>Yii::app()->baseUrl.'/images/list-remove.png',

			'deleteConfirmation'=> false,

			// Updates available seats and available vehicles
			'afterDelete'=>'afterVehicleDelete',
			),
)));
?>

</div><!--End related-vehicles -->

<?php echo CHtml::link('+ Add Vehicle','#',array('id'=>'add-vehicle-link')); ?>
<div id="add-vehicle" style="display:none" >

<?php

// Gridview with unrelated vehicles
$this->widget('TGridView', array(
	'id'=>'available-vehicle-grid',
  'dataProvider'=>$unrelatedVehicleDataProvider,
  'columns'=>array(

  	// Link column for vehicle names
  	array(
  		'class' =>'CLinkColumn',
  		'header'=> 'Name',
  		'labelExpression'=> '$data->name',
  		'urlExpression'=>'Yii::app()->createUrl("/vehicle/$data->id")'
		),

		//Other vehicle properties
		'type',
		'capacity',

		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{move}',
			'header'=>'Options',
			'viewButtonUrl'=>'Yii::app()->createUrl("/vehicle/view", ' .
					'array("id"=>$data["id"]))',

			'updateButtonUrl'=>'Yii::app()->createUrl("/vehicle/update", ' .
					'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',

				// Move button
			'buttons'=> array(
				'move'=>array(
						'label'=>Yii::t('ToolTips', 'Add Vehicle to Activity'),
						'imageUrl'=> Yii::app()->baseUrl.'/images/list-add.png',
						'url'=>'Yii::app()->createUrl("/vehicle/addToActivity", ' .
								'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',
//						'click'=>'function(link, success, data){if(success) ' .
//							'$.fn.yiiGridView.update("busy-vehicle-grid");}',
				),

			),
		),

)));

?>
</div><!-- search-form -->
