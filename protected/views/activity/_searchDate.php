<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
));?>

	<div class="row">
		<?php 
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
		    'model'=>$model,
		    'attribute'=>'date',
        'flat'=>false,
		    
		    // additional javascript options for the date picker plugin
		    'options'=>array(
		        'showAnim'=>'fold',
		        'dateFormat'=>'yy-mm-dd',
		    ),
		    
		    'htmlOptions'=>array('style'=>'width: 250px;'),
		));
		?>
		<span class="hint"><?php echo "Click to search by date"; ?></span>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(
						'Search',
						array('id'=>'activity-date-search')
						); 
		?>
	</div>
	

<?php $this->endWidget(); ?>

</div><!-- form -->
