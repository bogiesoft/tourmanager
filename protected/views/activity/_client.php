<?php
Yii::app()->clientScript->registerScript('Client', "
$('#add-client-link').click(function(){
	$('#add-client').toggle();
	return false;
});"
);
?>

<div id="related-clients">
<?php
// For some reason (bug?) I can't have this gridview as a TGridView object
// because some costume css is not applied.
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'related-client-grid',
  'dataProvider'=>$clientDataProvider,
  'columns'=>array(

  	// Link column for client names
  	array(
  		'class' =>'CLinkColumn',
  		'header'=> 'Name',
  		'labelExpression'=> '$data->name',
  		'urlExpression'=>'Yii::app()->createUrl("/client/$data->id")'
		),

		// Other client properties
		'arrived',
		'pax',
		'number_children',
		array(
			'name'=>'hotel_id',
			'value'=>'CHtml::encode($data->getHotelName())'
		),
		array(
			'name'=>'agency_id',
			'value'=>'CHtml::encode($data->getTravelAgencyName())'
		),
		array(
			'name'=>'country_id',
			'value'=>'CHtml::encode($data->getCountryName())',
		),
		array(
			'name' => CHtml::encode('totalDirectOrVoucher'),
			'value' =>'CHtml::encode($data->getTotalDirectOrVoucher())'
		),
		array(
			'name'=>CHtml::encode('debt'),
			'value'=>'$data->getDebt()',
		),
		array(
			'name'=>CHtml::encode('Prepaid'),
			'value'=>'$data->value_prepayed'
		),
		'notes',


		// Actions
		array(
			'class'=>'CButtonColumn',
			'template'=>'{markAsArrived}{update}{move}{delete}',
			'header'=>'Options',
			'htmlOptions'=>array('width'=>'70px'),

			'viewButtonUrl'=>'Yii::app()->createUrl("/client/view", ' .
					'array("id"=>$data["id"]))',
			'viewButtonLabel'=> Yii::t('ToolTips', 'View Client'),

			'updateButtonUrl'=>'Yii::app()->createUrl("/client/update", ' .
					'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',
			'updateButtonLabel'=>Yii::t('ToolTips', 'Update Client'),

			'deleteButtonUrl'=>'Yii::app()->createUrl("/client/removeFromActivity",' .
					' array("id"=>$data["id"], "activityId"=>'."$model->id".'))',
			'deleteButtonLabel'=>Yii::t('ToolTips', 'Remove Client'),
			'deleteButtonImageUrl'=>Yii::app()->baseUrl.'/images/list-remove.png',
			'deleteConfirmation'=> false,

			// Update available seats
			'afterDelete'=>'refreshOccupationRate',

			// Move button button
			'buttons'=> array(
				'move'=>array(
					'label'=>Yii::t('ToolTips', 'Copy/Move Client'),
					'imageUrl'=> Yii::app()->baseUrl.'/images/go-next.png',
					'url'=>'Yii::app()->createUrl("/client/move", array("id"=>$data["id"],' .
							' "activityId"=>'."$model->id".'))',
					),
				'markAsArrived'=>array(
					'label'=>Yii::t('ToolTips', 'Mark as Arrived'),
					'imageUrl'=> Yii::app()->baseUrl.'/images/mail-signed-verified.png',
					'url'=>'Yii::app()->createUrl("/client/markAsArrived", array("id"=>$data["id"],' .
							' "activityId"=>'."$model->id".'))',
				),
			),

		),
)));

// Extra detail about clients
$this->widget('TDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'totalAdults',
		'totalChildren',
		'totalPax',
		'totalCashDirect',
		'totalCashCredit',
		'totalCash',
	),
));
?>
</div><!-- End related clients -->


<?php echo CHtml::link('+ Add Clients','#',array('id'=>'add-client-link')); ?>
<div id="add-client" style="display:none" >

<?php
$this->widget('TGridView', array(
	'id'=>'unrelated-client-grid',
  'dataProvider'=>$client->search(true),
  'filter'=>$client,
	'columns'=>array(
		'name',
		'pax',
		'number_children',
		'telephone',
		array(
			'name'=>'hotel_id',
			'value'=>'CHtml::encode($data->getHotelName())'
		),
		array(
			'name'=>'agency_id',
			'value'=>'CHtml::encode($data->getTravelAgencyName())'
		),
		array(
			'name'=>'country_id',
			'value'=>'CHtml::encode($data->getCountryName())',
		),

		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{add}',
			'header'=>'Options',
			'updateButtonUrl'=>'Yii::app()->createUrl("/client/update", ' .
					'array("id"=>$data["id"], "activityId"=>'."$model->id".'))',
			'updateButtonLabel'=>Yii::t('ToolTips', 'Update Client'),

			// Update available seats
			'afterDelete'=>'refreshOccupationRate',

			// Move button
			'buttons'=> array(
				'add'=>array(
						'label'=>Yii::t('ToolTips', ' Add to Actvivity'),
						'imageUrl'=> Yii::app()->baseUrl.'/images/list-add.png',
						'url'=>'Yii::app()->createUrl(' .
								'"/client/copyToActivity", ' .
								'array("id"=>$data["id"], ' .
								'"newActivityId"=>'."$model->id".',' .
								'"activityId"=>'."$model->id".'))',
					),
			),
	))));

echo CHtml::link('+ Add New Client', Yii::app()->createUrl(
	"/client/create",
	array("activityId"=>$model->id
	)));
?>
</div><!-- end add-client -->
