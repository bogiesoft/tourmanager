<?php
$this->breadcrumbs=array(
	'Activities',
);

$this->menu=array(
	array('label'=>'Create Activity', 'url'=>array('create')),
	array('label'=>'Manage Activity', 'url'=>array('admin')),
);
?>



<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('activity-grid-index', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<h1>Activities</h1>
<div class="search-form">
<?php $this->renderPartial('_searchDate',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->


<!-- Flash messages -->

<?php if(Yii::app()->user->hasFlash('success')):?>
	<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success');?>
	</div>
<?php
	// Create fade effect
	Yii::app()->clientScript->registerScript(
	'fadeAndHideEffect',
	'$(".flash-success").animate({opacity: 1.0}, 5000).fadeOut("slow");'
	);
endif; ?>

<?php if(Yii::app()->user->hasFlash('error')):?>
	<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error');?>
	</div>
<?php
	// Create fade effect
	Yii::app()->clientScript->registerScript(
	'fadeAndHideEffect',
	'$(".flash-error").animate({opacity: 1.0}, 5000).fadeOut("slow");'
	);
endif; ?>

<!-- End flash messages -->


<?php $this->widget('TGridView', array(
	'id'=>'activity-grid-index',
	'dataProvider'=>$model->search(),
	'filter'=>$model,

  // This redraws the datepicker after an ajax update
/*
  'afterAjaxUpdate'=>"function(){jQuery('#".CHtml::activeId($model, 'date').
    "').datepicker({dateFormat: 'yy-mm-dd', showAnim: 'fold' })}",
*/

	'columns'=>array(
    'date',
		'time',
    array( // display 'Activity.type_id' using an expression
      'name'=>'type_id',
      'value'=>'$data->type->name',

      // A dropdown list for activity types filtering
      'filter'=>CHtml::listdata(
                  ActivityType::model()->findAll(),
                  'id',
                  'name'
              ),
     ),

    // Total pax column
    array(
	  'name' => 'Total Pax',
	   'value'=>'$data->totalPax',
	   'filter'=>false
	 ),

	// Completed column
	array(
	  'name'=>'completed',
	  'value'=>'$data->getCompletedValue()',

	  // A dropdown list for activity types filtering
	  'filter'=>array('1'=>'Yes', '0'=>'No'),
	),
	'notes',

    // Buttons
		array(
		  'class'=>'CButtonColumn',
		  'template'=>'{view}{update}{completed}{delete}',

		// Mark as completed button
		'buttons'=> array(
		 	'completed'=>array(
				'label'=>Yii::t('ToolTips', 'Mark as Completed'),
				'imageUrl'=> Yii::app()->baseUrl.'/images/mail-signed-verified.png',
					'url'=>'Yii::app()->createUrl("/activity/markAsCompleted", array("id"=>$data["id"]))',
					),
				),

		// Make room for the extra button
		'htmlOptions'=>array('style'=>"width: 70px;"),
		),
	),

));

?>



