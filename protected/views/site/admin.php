<?php $this->pageTitle=Yii::app()->name; ?>

<h1><?php echo "Administer TourManager";?></h1>

<div id="admin-links">

<ul>

<li><?php echo CHtml::link('Activities', Yii::app()->createUrl('activity/admin')); ?></li>
<li><?php echo CHtml::link('Activity Types', Yii::app()->createUrl('activityType/admin')); ?></li>
<li><?php echo CHtml::link('Clients', Yii::app()->createUrl('client/admin')); ?></li>
<li><?php echo CHtml::link('Hotels', Yii::app()->createUrl('hotel/admin')); ?></li>
<li><?php echo CHtml::link('Staff Members', Yii::app()->createUrl('staffMember/admin')); ?></li>
<li><?php echo CHtml::link('Staff Roles', Yii::app()->createUrl('staffRole/admin')); ?></li>
<li><?php echo CHtml::link('Travel Agencies', Yii::app()->createUrl('travelAgency/admin')); ?></li>
<li><?php echo CHtml::link('Vehicles', Yii::app()->createUrl('vehicle/admin')); ?></li>
<?php if (Yii::app()->authManager->checkAccess('Admin', Yii::app()->user->id)): ?>
<li><?php echo CHtml::link('Users', Yii::app()->createUrl('user/admin')); ?></li>
<li> <?php echo CHtml::link('Permissions', Yii::app()->createUrl('rights')); ?></li>
<?php endif; ?>

</ul>

</div> <!-- End admin-links -->
