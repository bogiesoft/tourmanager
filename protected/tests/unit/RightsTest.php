<?php
/*
 * Created on 9 de Set de 2011
 *
 * Copyright: António Lima <amrlima@gmail.com>
 * www.blog.amrlima.info
 * tourmanager
 */
  class RightsTest extends CTestCase
 {
 	public function testGetSuperUsers()
 	{
 		$authManager = Yii::app()->getAuthManager();
 		
 		$assignments = $authManager->getAssignmentsByItemName( Rights::module()->superuserName );

		$userIdList = array();
		foreach( $assignments as $userId=>$assignment )
			$userIdList[] = $userId;

		$criteria = new CDbCriteria();
		$criteria->addInCondition(Rights::module()->userIdColumn, $userIdList);

		$userClass = Rights::module()->userClass;
		$users = CActiveRecord::model($userClass)->findAll($criteria);
		//$users = $this->attachUserBehavior($users);

		$superusers = array();
		foreach( $users as $user )
			$superusers[] = $user->username;

 		$this->assertTrue(is_array($superusers));
 		$this->assertTrue(count($superusers) == 1);
 		//foreach($superusers as $user)
 		//$this->assertTrue(strcmp($superusers[0], 'Admin') == true);
 		
 		$this->assertTrue(strcmp($superusers[0], Yii::app()->user->name) == true);
 		
 	}
 	
 	
 }
 
?>
