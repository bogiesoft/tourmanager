<?php

class ClientController extends RController
{

	/**
	 * @var private property containing the associated Activity model instance.
	 */
	private $_activity = null;

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations

			//check to ensure valid activity context
			'activityContext + create removeFromActivity moveToActivity copyToActivity',
		);
	}

	/**
	 * In-class defined filter method, configured for use in the above
   * filters() method
   * It is called before the actionCreate() action method is run in
   * order to ensure a proper activity context
   */
	public function filterActivityContext($filterChain)
	{
		//set the project identifier based on either the GET or POST input
	  //request variables, since we allow both types for our actions
		$activityId = null;
		if(isset($_GET['activityId']))
			$activityId = $_GET['activityId'];

		else
			if(isset($_POST['activityId']))
				$activityId = $_POST['activityId'];

		$this->loadActivity($activityId);
		//complete the running of other filters and execute the requested action

		$filterChain->run();
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow', // allow authenticated user to perform actions
//				'actions'=>array(
//					'create',
//					'update',
//					'delete',
//					'move',
//					'copyToActivity',
//					'moveToActivity',
//					'removeFromActivity',
//					),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array(
//					'admin',
//				),
//
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		// Make the travelAgency and Hotel name appear instead of the id
		$model = $this->loadModel($id);

		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($activityId)
	{

		$baseUrl = Yii::app()->baseUrl;
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/views.js', CClientScript::POS_HEAD);

		$model=new Client;

		// CJuiAutoComplete does not work with CHtml::listdata, we have to
		// rely on this costum function to get values for hotels and agencies.
		// It sucks, I know...
		$agencies = TravelAgency::model()->getAllNames();
		$hotels = Hotel::model()->getAllNames();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Client']))
		{
			$model->activity_id = $activityId;

			// Get the hotel, create it if it's new and add id to Client
			$hotel = $this->createHotel();
			$model->hotel_id = $hotel->id;

			// Do the same for travel agency
			$agency = $this->createTravelAgency();
			$model->agency_id = $agency->id;
			
			$model->attributes=$_POST['Client'];
			
			if($model->save())
			{
				Yii::log("Successfully created new client: " . $model->id,
				  "info",
				  "application.controllers.ClientController"
			    );

				Yii::app()->user->setFlash('success', "Client '$model->name' added to
				activity");

				// Redirect to activity since we always create clients in context of
				// an Activity instance
				$this->redirect(array(
						'/activity/view',
						'id'=>$this->_activity->id,
						'activeTab'=>'clients'
						)
				);
			}
			// failled to associate client with activity
			else
			{
			  Yii::log("Failed to create Client",
				"warning",
				"application.controllers.ClientController"
				);

			  Yii::app()->user->setFlash('error', "There was an error while
				adding Client '$model->name' to activity");

			  // Replace ids by human readable names if save fails
			  if (isset($model->agency))
					$model->agency_id = $model->agency->name;

				if (isset($model->hotel))
					$model->hotel_id = $model->hotel->name;

			}
		}

		$this->render('create',array(
			'model'=>$model,
			'agencies'=>$agencies,
			'hotels'=>$hotels,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 * @param integer $activityId the id of the activity to redirect to after
	 * the update. If $activityId is 0 the redirect is to the client view
	 * Redirecting to client view means the user updated the client from the
	 * client view. Redirecting to the activity view means the user updated the
	 * client from the activity view.
	 */
	public function actionUpdate($id, $activityId = 0)
	{
		$baseUrl = Yii::app()->baseUrl;
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/views.js', CClientScript::POS_HEAD);

		//Get all agencies and hotels names
		$agencies = TravelAgency::model()->getAllNames();
		$hotels = Hotel::model()->getAllNames();

		$model=$this->loadModel($id);

		if (isset($model->hotel_id))
			$model->hotel_id = $model->hotel->name;

		if (isset($model->agency_id))
			$model->agency_id = $model->agency->name;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Client']))
		{

			// Get the hotel, create it if it's new and add id to Client
			$hotel = $this->createHotel();
			$model->hotel_id = $hotel->id;

			// Do the same for travel agency
			$agency = $this->createTravelAgency();
			$model->agency_id = $agency->id;

			$model->attributes=$_POST['Client'];

			if($model->save())
			{
				// Redirect to client view
				if ($activityId == 0)
					$this->redirect(array('view','id'=>$model->id));

				// Redirect to activity view
				else
					$this->redirect(array(
						'/activity/view',
						'id'=>$activityId,
						'activeTab'=>'clients'
						)
					);
			}


			// Replace ids by human readable names if save fails
			else
			{
			  // Replace ids by human readable names if save fails
			  if (isset($model->agency))
					$model->agency_id = $model->agency->name;

				if (isset($model->hotel))
					$model->hotel_id = $model->hotel->name;
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'agencies'=>$agencies,
			'hotels'=>$hotels,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if($this->loadModel($id)->delete())
			{
			  Yii::log("Successfully deleted client",
			    "info",
				"application.controllers.ClientController"
			    );
			}
			else
			{
			  Yii::log("Failed to delete Client",
				"warning",
				"application.controllers.ClientController"
				);
			}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Client');

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Client('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Client']))
			$model->attributes=$_GET['Client'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Calls a view that lists available activities where to move the client
	 * @param int $id client id
	 * @param int $activityId activity id
	 */
	public function actionMove($id, $activityId)
	{
		$model = $this->loadModel($id);

		// Find all Not completed activities and order them by date
		$criteria=new CDbCriteria;
		$criteria->compare('completed', 0); // Only not completed activities
		$criteria->order = 'date';
		// Don't list the activity with which the client is currently associated
		// with
		$criteria->compare('id', "<>".$activityId);

		$activities = new CActiveDataProvider('Activity', array(
			'criteria'=>$criteria,
		));

		$this->render('move', array(
			'model'=>$model,
			'activities'=>$activities,
			'activityId'=>$activityId,
		));
	}

	/**
	 * Executes the actual copying of clients from one activity to another
	 * Note that clients are not really copied but duplicated since we don't
	 * want to have the same client reservation in more then one activity
	 * Set a success or failure message and redirects to the calling activity
	 * @param int $id client id
	 * @param int $activityId activity id
	 * @param int $redirect the activity id to where to redirect
	 */
	public function actionCopyToActivity($id, $newActivityId)
	{
		$model = $this->loadModel($id);

		// Copy client
		$newModel = new Client;
		$newModel->setAttributes($model->getAttributes());

		//Reset values
		$newModel->value_payed_adult = 0.00;
		$newModel->value_payed_children = 0.00;
		$newModel->value_prepayed = 0.00;
		$newModel->arrived = 'No';

		// Set new activity_id, validate and update record
		$newModel->activity_id = $newActivityId;

			if($newModel->save())
			{
				Yii::app()->user->setFlash('success', "Client '".$newModel->name."'
					added to activity #$newActivityId");

				// Redirect is made to the activity we where previously working with
				$this->redirect(array(
					'/activity/view',
					'id'=>$this->_activity->id,
					'activeTab'=>'clients'
					));
			}

		// Failed association
		else
		{ //TODO we need to set a "fail" message and a corresponding div to the view
			Yii::app()->user->setFlash('error', "There was an error while moving '".
				$newModel->name . "'to activity #$newActivityId");
			$this->redirect(array(
				'/activity/view',
				'id'=>$this->_activity->id,
				'activeTab'=>'clients'
				));
		}
	}

	/**
	 * Move a client to another activity
	 * @param $id activity id
	 * @param $clientId client id
	 */
	public function actionMoveToActivity($id, $newActivityId)
	{
		$model = $this->loadModel($id);

		// Set as not arrived, just in case...
		$model->arrived = 'No';

		// Set new activity_id, validate and update record
		$model->activity_id = $newActivityId;

		// Associate and redirect to new old
		if($model->validate(array('activity_id')) && $model->update(array('activity_id')))
		{
			Yii::app()->user->setFlash('success', "'$model->name' moved to
				activity #". $newActivityId);

			// Redirect is made to the activity we where previously working with
			$this->redirect(array(
				'/activity/view',
				'id'=>$this->_activity->id,
				'activeTab'=>'clients'
				));
		}
		//Failure
		else
		{
			Yii::app()->user->setFlash('error', "There was an error while
				moving '$model->name' - to activity #". $newActivityId);

			$$this->redirect(array(
				'/activity/view',
				'id'=>$this->_activity->id,
				'activeTab'=>'clients'
				));
		}
	}

	/**
	 * Remove client from activity (does not delete the client)
	 * @param int $id activity id
	 * @param int $clientId client id
	 */
	public function actionRemoveFromActivity($id)
	{
		$model = $this->loadModel($id);

		// Disassociate client
		$model->activity_id = NULL;

		// Validate and update
		if($model->validate(array('activity_id')) && $model->update(array('activity_id')))
		{
			Yii::app()->user->setFlash('success', "'$model->name' removed from
			activity ");
			$this->redirect(array(
				'/activity/view',
				'id'=>$this->_activity->id,
				'activeTab'=>'clients'));
		}

		// Failled to disassociate
		else
		{
			Yii::app()->user->setFlash(
				'error',
				"There was an error while removing '$model->name'"
			);
			$this->redirect(array(
				'/activity/view',
				'id'=>$this->_activity->id,
				'activeTab'=>'clients'));
		}
	}

	/**
	 * Sets client as arrived
	 * @param int id client id
	 */
	public function actionMarkAsArrived($id, $activityId)
	{
		$model = $this->loadModel($id);

		$model->arrived = 'Yes';

		if ($model->validate(array('arrived')) && $model->update(array('arrived')))
		{
			Yii::app()->user->setFlash('success', "Client #$model->id marked as " .
					"arrived");
			$this->redirect(array(
				'/activity/view',
				'id'=>$activityId,
				'activeTab'=>'clients'));
		}

		else
		{
			Yii::app()->user->setFlash('error', "There was an error while marking ' .
					'client #$model->id as completed");
			$this->redirect(array(
				'/activity/view',
				'id'=>$activityId,
				'activeTab'=>'clients'));
		}

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Client::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Protected method to load the associated Activity model class
	 * @activity_id the primary identifier of the associated Activity
	 * @return object the Activity data model based on the primary key
	 */
	protected function loadActivity($activityId)
	{
		//if the actvity property is null, create it based on input id
		if($this->_activity===null)
		{
			$this->_activity=Activity::model()->findbyPk($activityId);
			if($this->_activity===null)
			{
				throw new CHttpException(404,'The requested activity does not exist.');
			}
		}

		return $this->_activity;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='client-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Creates a new Hotel with the Client _form
	 */
	protected function createHotel()
	{
		$model=new Hotel;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Client']['hotel_id']))
		{
			$model->name = $_POST['Client']['hotel_id'];
			// Only save if the name not new
			if ($model->isNewRecord)
				$model->save();
		}
		
		return $model;
	}

	/**
	 * Creates a new Hotel with the Client _form
	 */
	protected function createTravelAgency()
	{
		$model=new TravelAgency;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Client']['agency_id']))
		{
			$model->name = $_POST['Client']['agency_id'];
			// Only save if the name not new
			if ($model->isNewRecord)
				$model->save();
		}
		
		return $model;
	}

}
