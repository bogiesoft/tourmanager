<?php

class VehicleController extends RController
{
	
	/**
	 * @var private property containing the associated Activity model instance.
	 */
	private $_activity = null;
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
			'activityContext + addToActivity removeFromActivity',
		);
	}

	/**
	 * In-class defined filter method, configured for use in the above
   * filters() method
   * It is called before the actionCreate() action method is run in
   * order to ensure a proper activity context
   */
	public function filterActivityContext($filterChain)
	{
		//set the project identifier based on either the GET or POST input
	  //request variables, since we allow both types for our actions
		$activityId = null;
		if(isset($_GET['activityId']))
			$activityId = $_GET['activityId'];
		
		else
			if(isset($_POST['activityId']))
				$activityId = $_POST['activityId'];
		
		$this->loadActivity($activityId);
		//complete the running of other filters and execute the requested action
		
		$filterChain->run();
	} 
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array(
//					'create',
//					'update', 
//					'addToActivity', 
//					'removeFromActivity',
//					'index',
//					'view'
//				),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Vehicle;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Vehicle']))
		{
			$model->attributes=$_POST['Vehicle'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 * @param integer $activityId the id of the activity to redirect to after
	 * the update. If $activityId is 0 the redirect is to the vehicle view
	 * Redirecting to vehicle view means the user updated the vehicle from the
	 * vehicle view. Redirecting to the activity view means the user updated the
	 * vehicle from the activity view.
	 */
	public function actionUpdate($id, $activityId = 0)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Vehicle']))
		{
			$model->attributes=$_POST['Vehicle'];
			if($model->save())
			{
				// Redirect to client view 
				if ($activityId == 0)
					$this->redirect(array('view','id'=>$model->id));
				
				// Redirect to activity view
				else
					$this->redirect(array(
							'/activity/view', 
							'id'=>$activityId,
							'activeTab'=>'vehicles'
							)
					);
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Vehicle');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Vehicle('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Vehicle']))
			$model->attributes=$_GET['Vehicle'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Adds a staff member to an activity
	 * @param int $id vehicle id
	 */
	public function actionAddToActivity($id)
	{
		$model = $this->loadModel($id);
		
		//TODO whe are redirecting instead of just refreshing the grids... it has
		// to be done with AJAX, still have to lear it :(	
		if($model->associateVehicleWithActivity($this->_activity->id))
		{
				Yii::app()->user->setFlash('success', "Vehicle '$model->name' added to 
				activity");
				$this->redirect(array('/activity/view','id'=>$this->_activity->id, 'activeTab'=>'vehicles'));
		}
		
		else
		{
			Yii::app()->user->setFlash('error', "There was an error while 
				adding Vehicle '$model->name' to activity");
			$this->redirect(array('/activity/view','id'=>$this->_activity->id, 'activeTab'=>'vehicles'));
		}
	}
	
	/**
	 * Remove vehicle from activity
	 * @param int id Activity id
	 * @param int vehicleId vehicle id to dissociate from activity
	 */
	public function actionRemoveFromActivity($id)
	{
		$model = $this->loadModel($id);
		
   		// Disassociate
   		if($model->disassociateVehicleWithActivity($this->_activity->id))
   		{
			Yii::app()->user->setFlash('success', "Vehicle '$model->name' removed from 
				activity");
			$this->redirect(array('/activity/view','id'=>$this->_activity->id, 'activeTab'=>'vehicles'));
		}
		
		// Failed to disassociate
		else
		{
			Yii::app()->user->setFlash('error', "There was an error while 
				removing Vehicle '$model->name' from activity");
			$this->redirect(array('view','id'=>$id, 'activeTab'=>'vehicles'));
		}
	}
	

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Vehicle::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	/**
	 * Protected method to load the associated Activity model class
	 * @activity_id the primary identifier of the associated Activity
	 * @return object the Activity data model based on the primary key
	 */
	protected function loadActivity($activityId)
	{
		//if the actvity property is null, create it based on input id
		if($this->_activity===null)
		{
			$this->_activity=Activity::model()->findbyPk($activityId);
			if($this->_activity===null)
			{
				throw new CHttpException(404,'The requested activity does not exist.');
			}
		}
		
		return $this->_activity;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='vehicle-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
