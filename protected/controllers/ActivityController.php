<?php

class ActivityController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform actions
				'actions'=>array(
					'create',
					'update',
					'calendar',
					'showDay',
					'index',
					'view',
					'delete',
					'markAsCompleted',
					),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(
					'admin',
			),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id, $activeTab = 'basic')
	{
		$baseUrl = Yii::app()->baseUrl;
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/views.js', CClientScript::POS_HEAD);

		$model = $this->loadModel($id);

		// Get staff data providers
		$staffDataProvider = $model->getRelatedData('StaffMember');
		$unrelatedStaffDataProvider = $model->getUnrelatedData('StaffMember');

		// Get vehicle data providers
		$vehicleDataProvider = $model->getRelatedData('Vehicle');
		$unrelatedVehicleDataProvider = $model->getUnrelatedData('Vehicle');

		// Get Client data providers
		$clientDataProvider = new CActiveDataProvider('Client', array(
			'criteria'=>array(
				'condition'=>"activity_id = $model->id"
			),
		));

		// Get recent unrelated clients. The criteria are the following
		// activity_id != $model->id
		// date (create_time) between 2 weeks ago and today
		/*
		$clientCriteria = new CDbCriteria;
		$clientCriteria->addNotInCondition('activity_id', array($model->id));
   	$lastTwoWeeks = date('Y-m-d', strtotime('-2 week'));

		// +1 day includes today's clients
   	$today = date('Y-m-d', strtotime("+1 day"));
		$clientCriteria->addBetweenCondition('create_time', $lastTwoWeeks, $today);

		$unrelatedClientDataProvider = new CActiveDataProvider('Client', array(
			'criteria'=>$clientCriteria,
		));
		*/
		
		// For client search
		$client=new Client('searchRecent', true);
		$client->unsetAttributes();  // clear any default values
		if(isset($_GET['Client']))
			$client->attributes=$_GET['Client'];


		// Render the view... finally!
		$this->render('view',array(
			'model'=> $model,
			'vehicle_list'=>$model->vehicles,
			'staff_list'=>$model->staff_members,
			'vehicleDataProvider'=>$vehicleDataProvider,
			'unrelatedVehicleDataProvider'=>$unrelatedVehicleDataProvider,
			'staffDataProvider'=>$staffDataProvider,
			'unrelatedStaffDataProvider'=>$unrelatedStaffDataProvider,
			'clientDataProvider'=>$clientDataProvider,
			'client'=>$client,
			'activeTab'=>$activeTab, //Set active tab for CTabView
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Activity;

		// Get staff and activity types lists
		$staff_list = CHtml::listdata(
			StaffMember::model()->findAll(),
			'id',
			'name'
		);
		$vehicle_list = CHtml::listdata(
			Vehicle::model()->findAll(),
			'id',
			'name'
		);
		$activity_types = CHtml::listdata(
			ActivityType::model()->findAll(),
			'id',
			'name'
		);


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Activity']))
		{
			$model->attributes=$_POST['Activity'];
			if($model->save())
			{
			  Yii::log("Successfully created new activity: " . $model->id,
				"info",
				"application.controllers.ActivityController"
			    );
			  $this->redirect(array('view','id'=>$model->id));
			}
			else
			{
			  Yii::log("Failed to create Activity",
				"warning",
				"application.controllers.ActivityController"
				);
			}


		}

		$this->render('create',array(
			'model'=>$model,
			'activity_types'=>$activity_types,
			'vehicle_list'=>$vehicle_list,
			'staff_list'=>$staff_list,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Get extra data to list in create form
		$vehicles = Vehicle::model()->findAll();
		$staffMembers = StaffMember::model()->findAll();
		$activityTypes = ActivityType::model()->findAll();

		// List data
		$staff_list = CHtml::listdata($staffMembers, 'id', 'name');
		$vehicle_list = CHtml::listdata($vehicles, 'id', 'name');
		$activity_types = CHtml::listdata($activityTypes, 'id', 'name');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);



		if(isset($_POST['Activity']))
		{
			$model->attributes=$_POST['Activity'];

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
			'activity_types'=>$activity_types,
			'vehicle_list'=>$vehicle_list,
			'staff_list'=>$staff_list,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			if($this->loadModel($id)->delete())
			{
			  Yii::log("Successfully deleted activity",
				"info",
				"application.controllers.ActivityController"
			    );
			}
			else
			{
				Yii::log("Failed to create Activity",
				  "warning",
				  "application.controllers.ActivityController"
				);
			}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Activity('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Activity']))
			$model->attributes=$_GET['Activity'];

		$dataProvider=new CActiveDataProvider('Activity',
			array(
				'criteria'=>array(
					'order'=>'date DESC',
					),
			));


		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Activity('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Activity']))
			$model->attributes=$_GET['Activity'];

		$this->render('admin',array(
			'model'=>$model,
		));

	}

	/**
   * Displays a Activity Calendar
   */
	public function actionCalendar()
	{
	 /**
	  * If the user selected the month and year via drop down then it's set
	  * via post - otherwise they clicked a link, which means it's set by get.
	  * If the next / previous month links were clicked the get value will be filled
	  * otherwise it will be NULL (like clicking the calendar link in main menu) and the
	  * class will account for this and set the default current month and year.
	  */

    if (isset($_POST['month']) && isset($_POST['year']))
    {
     	$month = $_POST['month'];
     	$year = $_POST['year'];
    }
    else if(isset($_GET['month']) && isset($_GET['year']))
    {
     	$month = $_GET['month'];
     	$year = $_GET['year'];
    }

    else
    {
    	$month = null;
    	$year = null;
    }

    // Create calendar and set css file
    $calendar=new Calendar(
    	$month,
     	$year,
			Yii::app()->theme->baseUrl . '/css/calendar.css'
		);

     // Create links for the control menu
     $calendar->nextMonthUrl = $this->createUrl('activity/calendar', array(
         'month'=>$calendar->nextMonth,
         'year'=>$calendar->yearNextMonth)
     );
     $calendar->previousMonthUrl = $this->createUrl('activity/calendar', array(
         'month'=>$calendar->previousMonth,
         'year'=>$calendar->yearPreviousMonth)
     );
     $calendar->storePreviousLink = CHtml::link("< Previous Month", $calendar->previousMonthUrl);
     $calendar->storeNextLink = CHtml::link("Next Month >", $calendar->nextMonthUrl);
     $this->render('calendar',array('calendar'=>$calendar));
	}

	/**
	 * Displays a list of activities for a given date
	 * @param string $date to display activities
	 */
	public function actionShowDay($date = null)
	{
		// If the user gets here by typing the view name in the browser's nav bar
		// without specifying a day, set today as date to show activities
		if ($date === null)
			$date = date('Y-m-d', time());

		$dataProvider=new CActiveDataProvider('Activity', array(
				'criteria'=>array(
					'condition'=>"date = '$date'",
					'order'=>'time',
				)
			)
		);

		$this->render('showDay',array(
			'dataProvider'=>$dataProvider,
			'date'=>$date,
		));
	}

	/**
	 * Sets activity as completed
	 * @param int id activity id
	 */
	public function actionMarkAsCompleted($id)
	{
		$model = $this->loadModel($id);

		$model->completed = 1;

		if ($model->save())
		{
			Yii::app()->user->setFlash('success', "Activity $model->name marked as " .
					"completed");
			$this->redirect(array('index'));
		}

		else
		{
			Yii::app()->user->setFlash('error', "There was an error while marking ' .
					'$model->name as completed");
			$this->redirect(array('index'));
		}

	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Activity::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	public function getRedirectId()
	{
		return $this->_reditectId;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='activity-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
