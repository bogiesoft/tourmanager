-- --------------------------------------------------------

CREATE SCHEMA IF NOT EXISTS `tourmanager` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;

USE `tourmanager`;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`rel_activity_staff` (
  `activity_id` INT(11) NOT NULL ,
  `staff_member_id` INT(11) NOT NULL ,
  PRIMARY KEY (`activity_id`, `staff_member_id`) ,
  INDEX `tourmanager_activity_staff_45b57829` (`activity_id` ASC) ,
  INDEX `tourmanager_activity_staff_2804bd43` (`staff_member_id` ASC) ,
  CONSTRAINT `staffmember_id_refs_id_46661998`
    FOREIGN KEY (`staff_member_id` )
    REFERENCES `tourmanager`.`tbl_staff_member` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `activity_id_refs_id_5d7d2e20`
    FOREIGN KEY (`activity_id` )
    REFERENCES `tourmanager`.`tbl_activity` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_activity` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `date` DATE NOT NULL ,
  `time` TIME NOT NULL ,
  `type_id` INT(11) NOT NULL ,
  `completed` TINYINT(1) NOT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
	`notes` TEXT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `tourmanager_activity_777d41c8` (`type_id` ASC) ,
  CONSTRAINT `type_id_refs_id_3d68adff`
    FOREIGN KEY (`type_id` )
    REFERENCES `tourmanager`.`tbl_activity_type` (`id` ))
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_activity_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_client` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL ,
  `telephone` VARCHAR(32) NULL DEFAULT NULL ,
  `email` VARCHAR(256) NULL DEFAULT NULL ,
  `hotel_id` INT(11) NULL DEFAULT NULL ,
  `room_number` INT(11) NULL DEFAULT NULL ,
  `agency_id` INT(11) NULL DEFAULT NULL ,
  `pax` INT(11) NOT NULL ,
  `value_payed_adult` DECIMAL(10,2) NULL DEFAULT NULL ,
  `notes` TEXT NULL DEFAULT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  `country_id` INT(11) NULL DEFAULT NULL ,
  `number_children` INT(11) NULL DEFAULT '0' ,
  `value_payed_children` DECIMAL(10,2) NULL DEFAULT NULL ,
  `value_prepayed` DECIMAL(10,2) NULL DEFAULT NULL ,
  `create_user_id` INT(11) NULL DEFAULT NULL ,
  `paymment_type` INT(11) NOT NULL ,
  `voucher` VARCHAR(64) NULL DEFAULT NULL ,
  `activity_id` INT(11) NULL DEFAULT NULL ,
  `arrived` VARCHAR(6) NULL DEFAULT 'No' ,
  PRIMARY KEY (`id`) ,
  INDEX `tourmanager_client_27a8d263` (`hotel_id` ASC) ,
  INDEX `tourmanager_client_b162e9d` (`agency_id` ASC) ,
  INDEX `fk_tbl_client_tbl_country1` (`country_id` ASC) ,
  INDEX `fk_tbl_client_tbl_user1` (`create_user_id` ASC) ,
  INDEX `fk_tbl_client_tbl_activity1` (`activity_id` ASC) ,
  CONSTRAINT `agency_id_refs_id_198b9f25`
    FOREIGN KEY (`agency_id` )
    REFERENCES `tourmanager`.`tbl_travel_agency` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_client_tbl_activity1`
    FOREIGN KEY (`activity_id` )
    REFERENCES `tourmanager`.`tbl_activity` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_client_tbl_country1`
    FOREIGN KEY (`country_id` )
    REFERENCES `tourmanager`.`tbl_country` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_client_tbl_user1`
    FOREIGN KEY (`create_user_id` )
    REFERENCES `tourmanager`.`tbl_user` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `hotel_id_refs_id_1e63f9f1`
    FOREIGN KEY (`hotel_id` )
    REFERENCES `tourmanager`.`tbl_hotel` (`id` )
    ON DELETE SET NULL)
ENGINE = InnoDB
AUTO_INCREMENT = 29
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_hotel` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL ,
  `telephone` VARCHAR(32) NULL DEFAULT NULL ,
  `email` VARCHAR(256) NULL DEFAULT NULL ,
  `street` VARCHAR(256) NULL DEFAULT NULL ,
  `postal_code` VARCHAR(8) NULL DEFAULT NULL ,
  `city` VARCHAR(64) NULL DEFAULT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_country` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 172
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_staff_member` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL ,
  `freelancer` TINYINT(1) NULL DEFAULT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  `role_id` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_staff_member_tbl_staff_role1` (`role_id` ASC) ,
  CONSTRAINT `fk_tbl_staff_member_tbl_staff_role1`
    FOREIGN KEY (`role_id` )
    REFERENCES `tourmanager`.`tbl_staff_role` (`id` )
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_staff_role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_travel_agency` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL ,
  `telephone` VARCHAR(32) NULL DEFAULT NULL ,
  `email` VARCHAR(256) NULL DEFAULT NULL ,
  `street` VARCHAR(256) NULL DEFAULT NULL ,
  `postal_code` VARCHAR(8) NULL DEFAULT NULL ,
  `city` VARCHAR(64) NULL DEFAULT NULL ,
  `country_id` INT(11) NULL DEFAULT NULL ,
  `mobile_phone` VARCHAR(32) NULL DEFAULT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_tbl_travel_agency_tbl_country1` (`country_id` ASC) ,
  CONSTRAINT `fk_tbl_travel_agency_tbl_country1`
    FOREIGN KEY (`country_id` )
    REFERENCES `tourmanager`.`tbl_country` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_vehicle` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(64) NOT NULL ,
  `type` VARCHAR(64) NOT NULL ,
  `license` VARCHAR(32) NULL DEFAULT NULL ,
  `capacity` INT(11) NOT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`rel_activity_vehicle` (
  `activity_id` INT(11) NOT NULL ,
  `vehicle_id` INT(11) NOT NULL ,
  PRIMARY KEY (`activity_id`, `vehicle_id`) ,
  INDEX `fk_rel_activity_vehicle_tbl_activity1` (`activity_id` ASC) ,
  INDEX `fk_rel_activity_vehicle_tbl_vehicle1` (`vehicle_id` ASC) ,
  CONSTRAINT `fk_rel_activity_vehicle_tbl_activity1`
    FOREIGN KEY (`activity_id` )
    REFERENCES `tourmanager`.`tbl_activity` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rel_activity_vehicle_tbl_vehicle1`
    FOREIGN KEY (`vehicle_id` )
    REFERENCES `tourmanager`.`tbl_vehicle` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(256) NOT NULL ,
  `password` VARCHAR(256) NOT NULL ,
  `email` VARCHAR(256) NULL DEFAULT NULL ,
  `name` VARCHAR(256) NULL DEFAULT NULL ,
  `last_login_time` DATETIME NULL DEFAULT NULL ,
  `create_time` DATETIME NULL DEFAULT NULL ,
  `create_user_id` INT(11) NULL DEFAULT NULL ,
  `update_time` DATETIME NULL DEFAULT NULL ,
  `update_user_id` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_rights` (
  `itemname` VARCHAR(64) NOT NULL ,
  `type` INT(11) NOT NULL ,
  `weight` INT(11) NOT NULL ,
  PRIMARY KEY (`itemname`) ,
  CONSTRAINT `tbl_rights_ibfk_1`
    FOREIGN KEY (`itemname` )
    REFERENCES `tourmanager`.`tbl_authitem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_authitemchild` (
  `parent` VARCHAR(64) NOT NULL ,
  `child` VARCHAR(64) NOT NULL ,
  PRIMARY KEY (`parent`, `child`) ,
  INDEX `child` (`child` ASC) ,
  CONSTRAINT `tbl_authitemchild_ibfk_1`
    FOREIGN KEY (`parent` )
    REFERENCES `tourmanager`.`tbl_authitem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `tbl_authitemchild_ibfk_2`
    FOREIGN KEY (`child` )
    REFERENCES `tourmanager`.`tbl_authitem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_authitem` (
  `name` VARCHAR(64) NOT NULL ,
  `type` INT(11) NOT NULL ,
  `description` TEXT NULL DEFAULT NULL ,
  `bizrule` TEXT NULL DEFAULT NULL ,
  `data` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`name`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE  TABLE IF NOT EXISTS `tourmanager`.`tbl_authassignment` (
  `itemname` VARCHAR(64) NOT NULL ,
  `userid` VARCHAR(64) NOT NULL ,
  `bizrule` TEXT NULL DEFAULT NULL ,
  `data` TEXT NULL DEFAULT NULL ,
  PRIMARY KEY (`itemname`, `userid`) ,
  CONSTRAINT `tbl_authassignment_ibfk_1`
    FOREIGN KEY (`itemname` )
    REFERENCES `tourmanager`.`tbl_authitem` (`name` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

--
-- Data for table `tbl_authitem`
--

INSERT INTO `tbl_authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('Activity.Admin', 0, NULL, NULL, 'N;'),
('Activity.Calendar', 0, NULL, NULL, 'N;'),
('Activity.Create', 0, NULL, NULL, 'N;'),
('Activity.Delete', 0, NULL, NULL, 'N;'),
('Activity.Index', 0, NULL, NULL, 'N;'),
('Activity.Management', 1, 'Manage Activities', NULL, 'N;'),
('Activity.MarkAsCompleted', 0, NULL, NULL, 'N;'),
('Activity.ShowDay', 0, NULL, NULL, 'N;'),
('Activity.Update', 0, NULL, NULL, 'N;'),
('Activity.View', 0, NULL, NULL, 'N;'),
('Activity.Viewing', 1, 'View Activities', NULL, 'N;'),
('ActivityType.Admin', 0, NULL, NULL, 'N;'),
('ActivityType.Create', 0, NULL, NULL, 'N;'),
('ActivityType.Delete', 0, NULL, NULL, 'N;'),
('ActivityType.Index', 0, NULL, NULL, 'N;'),
('ActivityType.Management', 1, 'Manage Activity Types', NULL, 'N;'),
('ActivityType.Update', 0, NULL, NULL, 'N;'),
('ActivityType.View', 0, NULL, NULL, 'N;'),
('ActivityType.Viewing', 1, 'View Activity Types', NULL, 'N;'),
('Admin', 2, 'Site Administrator', NULL, 'N;'),
('Authenticated', 2, 'Authenticated users', 'return !Yii::app()->user->isGuest;', 'N;'),
('Client.Admin', 0, NULL, NULL, 'N;'),
('Client.CopyToActivity', 0, NULL, NULL, 'N;'),
('Client.Create', 0, NULL, NULL, 'N;'),
('Client.Delete', 0, NULL, NULL, 'N;'),
('Client.Index', 0, NULL, NULL, 'N;'),
('Client.Management', 1, 'Manage Clients', NULL, 'N;'),
('Client.MarkAsArrived', 0, NULL, NULL, 'N;'),
('Client.Move', 0, NULL, NULL, 'N;'),
('Client.MoveToActivity', 0, NULL, NULL, 'N;'),
('Client.RemoveFromActivity', 0, NULL, NULL, 'N;'),
('Client.Report', 0, 'Client.Report', NULL, 'N;'),
('Client.Update', 0, NULL, NULL, 'N;'),
('Client.View', 0, NULL, NULL, 'N;'),
('Client.Viewing', 1, 'View Clients', NULL, 'N;'),
('Guest', 2, 'Guest users', 'return Yii::app()->user->isGuest;', 'N;'),
('Hotel.Admin', 0, NULL, NULL, 'N;'),
('Hotel.Create', 0, NULL, NULL, 'N;'),
('Hotel.Delete', 0, NULL, NULL, 'N;'),
('Hotel.Index', 0, NULL, NULL, 'N;'),
('Hotel.Management', 1, 'Manage hotels', NULL, 'N;'),
('Hotel.Update', 0, NULL, NULL, 'N;'),
('Hotel.View', 0, NULL, NULL, 'N;'),
('Hotel.Viewing', 1, 'View hotels', NULL, 'N;'),
('Site.*', 1, NULL, NULL, 'N;'),
('Site.About', 0, NULL, NULL, 'N;'),
('Site.Admin', 0, NULL, NULL, 'N;'),
('Site.Contact', 0, NULL, NULL, 'N;'),
('Site.Error', 0, NULL, NULL, 'N;'),
('Site.Guest', 1, 'Guests permissions', NULL, 'N;'),
('Site.Index', 0, NULL, NULL, 'N;'),
('Site.Login', 0, NULL, NULL, 'N;'),
('Site.Logout', 0, NULL, NULL, 'N;'),
('Site.Management', 1, 'Administer Site', NULL, 'N;'),
('Site.Viewing', 1, 'View the site content', NULL, 'N;'),
('StaffMember.AddToActivity', 0, NULL, NULL, 'N;'),
('StaffMember.Admin', 0, NULL, NULL, 'N;'),
('StaffMember.Create', 0, NULL, NULL, 'N;'),
('StaffMember.Delete', 0, NULL, NULL, 'N;'),
('StaffMember.Index', 0, NULL, NULL, 'N;'),
('StaffMember.Management', 1, 'Manage Staff Members', NULL, 'N;'),
('StaffMember.RemoveFromActivity', 0, NULL, NULL, 'N;'),
('StaffMember.Update', 0, NULL, NULL, 'N;'),
('StaffMember.View', 0, NULL, NULL, 'N;'),
('StaffMember.Viewing', 1, 'View  Staff Members', NULL, 'N;'),
('StaffRole.Admin', 0, NULL, NULL, 'N;'),
('StaffRole.Create', 0, NULL, NULL, 'N;'),
('StaffRole.Delete', 0, NULL, NULL, 'N;'),
('StaffRole.Index', 0, NULL, NULL, 'N;'),
('StaffRole.Management', 1, 'Manage Staff Roles', NULL, 'N;'),
('StaffRole.Update', 0, NULL, NULL, 'N;'),
('StaffRole.View', 0, NULL, NULL, 'N;'),
('StaffRole.Viewing', 1, 'View Staff Roles', NULL, 'N;'),
('TravelAgency.Admin', 0, NULL, NULL, 'N;'),
('TravelAgency.Create', 0, NULL, NULL, 'N;'),
('TravelAgency.Delete', 0, NULL, NULL, 'N;'),
('TravelAgency.Index', 0, NULL, NULL, 'N;'),
('TravelAgency.Management', 1, 'Manage Travel Agencies', NULL, 'N;'),
('TravelAgency.Update', 0, NULL, NULL, 'N;'),
('TravelAgency.View', 0, NULL, NULL, 'N;'),
('TravelAgency.Viewing', 1, 'View Travel Agencies', NULL, 'N;'),
('User.Admin', 0, NULL, NULL, 'N;'),
('User.ChangePassword', 0, NULL, NULL, 'N;'),
('User.Create', 0, NULL, NULL, 'N;'),
('User.Delete', 0, NULL, NULL, 'N;'),
('User.Index', 0, NULL, NULL, 'N;'),
('User.Management', 1, 'Manage users', NULL, 'N;'),
('User.Update', 0, NULL, NULL, 'N;'),
('User.View', 0, NULL, NULL, 'N;'),
('User.Viewing', 1, 'User viewing', NULL, 'N;'),
('Vehicle.AddToActivity', 0, NULL, NULL, 'N;'),
('Vehicle.Admin', 0, NULL, NULL, 'N;'),
('Vehicle.Create', 0, NULL, NULL, 'N;'),
('Vehicle.Delete', 0, NULL, NULL, 'N;'),
('Vehicle.Index', 0, NULL, NULL, 'N;'),
('Vehicle.Management', 1, 'Manage Vehicles', NULL, 'N;'),
('Vehicle.RemoveFromActivity', 0, NULL, NULL, 'N;'),
('Vehicle.Update', 0, NULL, NULL, 'N;'),
('Vehicle.View', 0, NULL, NULL, 'N;'),
('Vehicle.Viewing', 1, 'View Vehicles', NULL, 'N;');

-- --------------------------------------------------------

--
-- Data for table `tbl_authitemchild`
--

INSERT INTO `tbl_authitemchild` (`parent`, `child`) VALUES
('Activity.Management', 'Activity.Admin'),
('Activity.Viewing', 'Activity.Calendar'),
('Activity.Management', 'Activity.Create'),
('Activity.Management', 'Activity.Delete'),
('Activity.Viewing', 'Activity.Index'),
('Authenticated', 'Activity.Management'),
('Activity.Management', 'Activity.MarkAsCompleted'),
('Activity.Viewing', 'Activity.ShowDay'),
('Activity.Management', 'Activity.Update'),
('Activity.Viewing', 'Activity.View'),
('Authenticated', 'Activity.Viewing'),
('ActivityType.Management', 'ActivityType.Admin'),
('ActivityType.Management', 'ActivityType.Create'),
('ActivityType.Management', 'ActivityType.Delete'),
('Authenticated', 'ActivityType.Delete'),
('ActivityType.Viewing', 'ActivityType.Index'),
('Authenticated', 'ActivityType.Management'),
('ActivityType.Management', 'ActivityType.Update'),
('Authenticated', 'ActivityType.Update'),
('ActivityType.Viewing', 'ActivityType.View'),
('Authenticated', 'ActivityType.Viewing'),
('Admin', 'Authenticated'),
('Client.Management', 'Client.Admin'),
('Client.Management', 'Client.CopyToActivity'),
('Client.Management', 'Client.Create'),
('Client.Management', 'Client.Delete'),
('Client.Viewing', 'Client.Index'),
('Authenticated', 'Client.Management'),
('Client.Management', 'Client.Move'),
('Client.Management', 'Client.MoveToActivity'),
('Client.Management', 'Client.RemoveFromActivity'),
('Authenticated', 'Client.Report'),
('Client.Management', 'Client.Update'),
('Client.Viewing', 'Client.View'),
('Authenticated', 'Client.Viewing'),
('Client.Management', 'Client.MarkAsArrived'),
('Authenticated', 'Guest'),
('Hotel.Management', 'Hotel.Admin'),
('Hotel.Management', 'Hotel.Create'),
('Hotel.Management', 'Hotel.Delete'),
('Hotel.Viewing', 'Hotel.Index'),
('Authenticated', 'Hotel.Management'),
('Hotel.Management', 'Hotel.Update'),
('Hotel.Viewing', 'Hotel.View'),
('Authenticated', 'Hotel.Viewing'),
('Site.Viewing', 'Site.About'),
('Site.Management', 'Site.Admin'),
('Site.Viewing', 'Site.Contact'),
('Site.Guest', 'Site.Error'),
('Guest', 'Site.Guest'),
('Site.Viewing', 'Site.Guest'),
('Site.Viewing', 'Site.Index'),
('Site.Guest', 'Site.Login'),
('Site.Viewing', 'Site.Logout'),
('Admin', 'Site.Management'),
('Authenticated', 'Site.Management'),
('Authenticated', 'Site.Viewing'),
('StaffMember.Management', 'StaffMember.AddToActivity'),
('StaffMember.Management', 'StaffMember.Admin'),
('StaffMember.Management', 'StaffMember.Create'),
('StaffMember.Management', 'StaffMember.Delete'),
('StaffMember.Viewing', 'StaffMember.Index'),
('Authenticated', 'StaffMember.Management'),
('StaffMember.Management', 'StaffMember.RemoveFromActivity'),
('StaffMember.Management', 'StaffMember.Update'),
('StaffMember.Viewing', 'StaffMember.View'),
('Authenticated', 'StaffMember.Viewing'),
('StaffRole.Management', 'StaffRole.Admin'),
('StaffRole.Management', 'StaffRole.Create'),
('StaffRole.Management', 'StaffRole.Delete'),
('StaffRole.Viewing', 'StaffRole.Index'),
('Authenticated', 'StaffRole.Management'),
('StaffRole.Management', 'StaffRole.Update'),
('StaffRole.Viewing', 'StaffRole.View'),
('Authenticated', 'StaffRole.Viewing'),
('TravelAgency.Management', 'TravelAgency.Admin'),
('TravelAgency.Management', 'TravelAgency.Create'),
('TravelAgency.Management', 'TravelAgency.Delete'),
('TravelAgency.Viewing', 'TravelAgency.Index'),
('Authenticated', 'TravelAgency.Management'),
('TravelAgency.Management', 'TravelAgency.Update'),
('TravelAgency.Viewing', 'TravelAgency.View'),
('Authenticated', 'TravelAgency.Viewing'),
('User.Management', 'User.Admin'),
('User.Management', 'User.Create'),
('User.Management', 'User.Index'),
('Admin', 'User.Management'),
('User.Management', 'User.Update'),
('User.Viewing', 'User.View'),
('Authenticated', 'User.Viewing'),
('Vehicle.Management', 'Vehicle.AddToActivity'),
('Vehicle.Management', 'Vehicle.Admin'),
('User.Management', 'User.ChangePassword'),
('Vehicle.Management', 'Vehicle.Create'),
('User.Management', 'User.Delete'),
('Vehicle.Management', 'Vehicle.Delete'),
('Vehicle.Viewing', 'Vehicle.Index'),
('Authenticated', 'Vehicle.Management'),
('Vehicle.Management', 'Vehicle.RemoveFromActivity'),
('Vehicle.Management', 'Vehicle.Update'),
('Vehicle.Viewing', 'Vehicle.View'),
('Authenticated', 'Vehicle.Viewing');

-- --------------------------------------------------------

--
-- Data for table `tbl_country`
--

INSERT INTO `tbl_country` (`id`, `name`, `create_time`, `update_time`) VALUES
(1, 'Afghanistan', NULL, NULL),
(2, 'Albania', NULL, NULL),
(3, 'Algeria', NULL, NULL),
(4, 'Andorra', NULL, NULL),
(5, 'Angola', NULL, NULL),
(6, 'Argentina', NULL, NULL),
(7, 'Armenia', NULL, NULL),
(8, 'Australia', NULL, NULL),
(9, 'Austria', NULL, NULL),
(10, 'Belarus', NULL, NULL),
(11, 'Belgium', NULL, NULL),
(12, 'Benin', NULL, NULL),
(13, 'Bermuda', NULL, NULL),
(14, 'Bolivia', NULL, NULL),
(15, 'Bosnia and Herzegovina', NULL, NULL),
(16, 'Botswana', NULL, NULL),
(17, 'Brazil', NULL, NULL),
(18, 'British Virgin Islands', NULL, NULL),
(19, 'Bulgaria', NULL, NULL),
(20, 'Cameroon', NULL, NULL),
(21, 'Canada', NULL, NULL),
(22, 'Cape Verde', NULL, NULL),
(23, 'Cayman Islands', NULL, NULL),
(24, 'Central African Republic', NULL, NULL),
(25, 'Chad', NULL, NULL),
(26, 'Chile', NULL, NULL),
(27, 'China', NULL, NULL),
(28, 'Christmas Island', NULL, NULL),
(29, 'Colombia', NULL, NULL),
(30, 'Congo', NULL, NULL),
(31, 'Costa Rica', NULL, NULL),
(32, 'Croatia', NULL, NULL),
(33, 'Cuba', NULL, NULL),
(34, 'Cyprus', NULL, NULL),
(35, 'Czech Republic', NULL, NULL),
(36, 'Democratic Republic of the Congo', NULL, NULL),
(37, 'Denmark', NULL, NULL),
(38, 'Dominican Republic', NULL, NULL),
(39, 'East Timor', NULL, NULL),
(40, 'Ecuador', NULL, NULL),
(41, 'Egypt', NULL, NULL),
(42, 'El Salvador', NULL, NULL),
(43, 'Estonia', NULL, NULL),
(44, 'Ethiopia', NULL, NULL),
(45, 'Faeroe Islands', NULL, NULL),
(46, 'Finland', NULL, NULL),
(47, 'Macedonia', NULL, NULL),
(48, 'France', NULL, NULL),
(49, 'French Guiana', NULL, NULL),
(50, 'French Polynesia', NULL, NULL),
(51, 'Georgia', NULL, NULL),
(52, 'Germany', NULL, NULL),
(53, 'Ghana', NULL, NULL),
(54, 'Gibraltar', NULL, NULL),
(55, 'Greece', NULL, NULL),
(56, 'Greenland', NULL, NULL),
(57, 'Guatemala', NULL, NULL),
(58, 'Guinea', NULL, NULL),
(59, 'Guinea-Bissau', NULL, NULL),
(60, 'Guyana', NULL, NULL),
(61, 'Haiti', NULL, NULL),
(62, 'Honduras', NULL, NULL),
(63, 'Hong Kong', NULL, NULL),
(64, 'Hungary', NULL, NULL),
(65, 'Iceland', NULL, NULL),
(66, 'India', NULL, NULL),
(67, 'Indonesia', NULL, NULL),
(68, 'Iran', NULL, NULL),
(69, 'Iraq', NULL, NULL),
(70, 'Ireland', NULL, NULL),
(71, 'Israel', NULL, NULL),
(72, 'Italy', NULL, NULL),
(73, 'Jamaica', NULL, NULL),
(74, 'Japan', NULL, NULL),
(75, 'Jordan', NULL, NULL),
(76, 'Kazakhstan', NULL, NULL),
(77, 'Kenya', NULL, NULL),
(78, 'Kuwait', NULL, NULL),
(79, 'Latvia', NULL, NULL),
(80, 'Lebanon', NULL, NULL),
(81, 'Lesotho', NULL, NULL),
(82, 'Liberia', NULL, NULL),
(83, 'Libya', NULL, NULL),
(84, 'Liechtenstein', NULL, NULL),
(85, 'Lithuania', NULL, NULL),
(86, 'Luxembourg', NULL, NULL),
(87, 'Macau', NULL, NULL),
(88, 'Madagascar', NULL, NULL),
(89, 'Malaysia', NULL, NULL),
(90, 'Maldives', NULL, NULL),
(91, 'Mali', NULL, NULL),
(92, 'Malta', NULL, NULL),
(93, 'Mauritania', NULL, NULL),
(94, 'Mexico', NULL, NULL),
(95, 'Micronesia', NULL, NULL),
(96, 'Moldova', NULL, NULL),
(97, 'Monaco', NULL, NULL),
(98, 'Mongolia', NULL, NULL),
(99, 'Montserrat', NULL, NULL),
(100, 'Morocco', NULL, NULL),
(101, 'Mozambique', NULL, NULL),
(102, 'Myanmar', NULL, NULL),
(103, 'Namibia', NULL, NULL),
(104, 'Nepal', NULL, NULL),
(105, 'Netherlands', NULL, NULL),
(106, 'New Zealand', NULL, NULL),
(107, 'Nicaragua', NULL, NULL),
(108, 'Niger', NULL, NULL),
(109, 'Nigeria', NULL, NULL),
(110, 'Norfolk Island', NULL, NULL),
(111, 'North Korea', NULL, NULL),
(112, 'Norway', NULL, NULL),
(113, 'Oman', NULL, NULL),
(114, 'Pakistan', NULL, NULL),
(115, 'Palau', NULL, NULL),
(116, 'Panama', NULL, NULL),
(117, 'Papua New Guinea', NULL, NULL),
(118, 'Paraguay', NULL, NULL),
(119, 'Peru', NULL, NULL),
(120, 'Philippines', NULL, NULL),
(121, 'Poland', NULL, NULL),
(122, 'Portugal', NULL, NULL),
(123, 'Puerto Rico', NULL, NULL),
(124, 'Qatar', NULL, NULL),
(125, 'Reunion', NULL, NULL),
(126, 'Romania', NULL, NULL),
(127, 'Russia', NULL, NULL),
(128, 'Rwanda', NULL, NULL),
(129, 'Sao Tome and Principe', NULL, NULL),
(130, 'Saint Helena', NULL, NULL),
(131, 'San Marino', NULL, NULL),
(132, 'Saudi Arabia', NULL, NULL),
(133, 'Senegal', NULL, NULL),
(134, 'Seychelles', NULL, NULL),
(135, 'Sierra Leone', NULL, NULL),
(136, 'Singapore', NULL, NULL),
(137, 'Slovakia', NULL, NULL),
(138, 'Slovenia', NULL, NULL),
(139, 'Somalia', NULL, NULL),
(140, 'South Africa', NULL, NULL),
(141, 'South Korea', NULL, NULL),
(142, 'Spain', NULL, NULL),
(143, 'Sudan', NULL, NULL),
(144, 'Suriname', NULL, NULL),
(145, 'Sweden', NULL, NULL),
(146, 'Switzerland', NULL, NULL),
(147, 'Syria', NULL, NULL),
(148, 'Taiwan', NULL, NULL),
(149, 'Tanzania', NULL, NULL),
(150, 'Thailand', NULL, NULL),
(151, 'The Bahamas', NULL, NULL),
(152, 'Togo', NULL, NULL),
(153, 'Tonga', NULL, NULL),
(154, 'Trinidad and Tobago', NULL, NULL),
(155, 'Tunisia', NULL, NULL),
(156, 'Turkey', NULL, NULL),
(157, 'Virgin Islands', NULL, NULL),
(158, 'Uganda', NULL, NULL),
(159, 'Ukraine', NULL, NULL),
(160, 'United Arab Emirates', NULL, NULL),
(161, 'United Kingdom', NULL, NULL),
(162, 'United States', NULL, NULL),
(163, 'Uruguay', NULL, NULL),
(164, 'Uzbekistan', NULL, NULL),
(165, 'Vatican City', NULL, NULL),
(166, 'Venezuela', NULL, NULL),
(167, 'Vietnam', NULL, NULL),
(168, 'Yemen', NULL, NULL),
(169, 'Yugoslavia', NULL, NULL),
(170, 'Zambia', NULL, NULL),
(171, 'Zimbabwe', NULL, NULL);

-- --------------------------------------------------------

--
-- Data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `email`, `name`) VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'NULL', 'Admin');
