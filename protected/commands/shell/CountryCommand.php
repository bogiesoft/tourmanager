<?php
/*
 * Created on 15 de Jun de 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 class CountryCommand extends CConsoleCommand
 {
 	private $_countrys = array('Afghanistan', 'Albania', 'Algeria', 'Andorra',
    'Angola', 'Argentina', 'Armenia', 'Australia', 'Austria', 'Belarus', 'Belgium',
    'Benin', 'Bermuda', 'Bolivia', 'Bosnia and Herzegovina', 'Botswana', 'Brazil',
    'British Virgin Islands', 'Bulgaria', 'Cameroon', 'Canada', 'Cape Verde',
    'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China',
    'Christmas Island', 'Colombia', 'Congo', 'Costa Rica', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic',
    'Democratic Republic of the Congo', 'Denmark', 'Dominican Republic',
    'East Timor', 'Ecuador', 'Egypt', 'El Salvador', 'Estonia', 'Ethiopia', 'Faeroe Islands', 'Finland',
    'Macedonia', 'France', 'French Guiana', 'French Polynesia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar',
    'Greece', 'Greenland', 'Guatemala', 'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Honduras', 'Hong Kong', 'Hungary',
    'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica',
    'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kuwait', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 
	'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macau', 'Madagascar', 'Malaysia', 'Maldives', 'Mali', 'Malta',
    'Mauritania', 'Mexico', 'Micronesia', 'Moldova','Monaco', 'Mongolia', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia',
    'Nepal', 'Netherlands', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Norfolk Island', 'North Korea',
    'Norway', 'Oman', 'Pakistan', 'Palau', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru',
    'Philippines', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar',
    'Reunion', 'Romania', 'Russia', 'Rwanda', 'Sao Tome and Principe', 'Saint Helena',
    'San Marino', 'Saudi Arabia', 'Senegal', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia',
    'Somalia', 'South Africa', 'South Korea', 'Spain', 'Sudan', 'Suriname', 'Sweden',
    'Switzerland', 'Syria', 'Taiwan', 'Tanzania', 'Thailand', 'The Bahamas',
    'Togo', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Virgin Islands', 'Uganda',
    'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'Uruguay', 'Uzbekistan',
    'Vatican City', 'Venezuela', 'Vietnam', 'Yemen', 'Yugoslavia', 'Zambia', 'Zimbabwe');
 	
 	public function getHelp()
	{
		return <<<EOD
  USAGE
    country
  DESCRIPTION
    This command populates the country database table with a set of countrys
EOD;
	}
 	/**
	* Execute the action.
	* @param array command line parameters specific for this command
	*/
	public function run($args)
 	{
 		$countryName = "";
 		foreach ($this->_countrys as $countryName)
 		{
 			$c = new Country();
 			$c->name = $countryName;
 			$c->save();
 			echo "Adding $countryName ...\n";
 		}
 		echo "Done!\n";
 			
 	}
 	
 }
 
 
 
?>