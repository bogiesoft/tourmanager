<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'TourManager',
	'theme'=> 'greentour',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.*',
		'application.modules.rights.*',
		'application.modules.rights.components.*',
	),

		'modules' => array(
			'rights'=>array(
				'userIdColumn'=>'id',
				'userNameColumn'=>'username',
				'enableBizRuleData'=>true,
				'userClass'=>'User',
				'superuserName'=>'admin',
				'authenticatedName'=>'Authenticated',
				'appLayout'=>'webroot.themes.greentour.views.layouts.main'
				),

	   		// uncomment the following to enable the Gii tool
			'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'5665&gnome',
			 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
				'ipFilters'=>array('127.0.0.1','::1'),
			),
	),

	// application components
	'components'=>array(
		'user'=>array(
			'class'=>'RWebUser', // Allows super users access implicitly.
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// Set pager widget css file
		'widgetFactory'=>array(
			'widgets'=>array(
				'CLinkPager'=>array(
					'cssFile'=> Yii::app()->theme->baseUrl . '/css/main.css',
				),
			),
		),

		// Authorization manager
		'authManager'=>array(
			'class'=>'RDbAuthManager', // Provides support authorization item sorting.
			'connectionID'=>'db',
			'itemTable'=>'tbl_authitem',
	    	'itemChildTable'=>'tbl_authitemchild',
	    	'assignmentTable'=>'tbl_authassignment',
	    	'rightsTable'=>'tbl_rights',
			'defaultRoles'=>array('Guest', 'Authenticated'),
		),

		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			//'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=tour_manager',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '5665gnome',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),

				array(
					'class'=>'CFileLogRoute',
					'levels'=>'info',
					'logFile'=>'infoMessages.log',
				),

				// uncomment the following to show log messages on web pages
//				array(
//					'class'=>'CWebLogRoute',
//				),

			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'amrlima@gmail.com',
	),
);
