<?php
Yii::import('zii.widgets.grid.CGridView');

class TGridView extends CGridView
{
	
	/**
	 * Initialize cssFile
	 */
	public function init()
	{
		$this->cssFile = Yii::app()->theme->baseUrl . '/css/tgridview.css';
		parent::init();
	}
	
}

?>
