<?php

Yii::import('zii.widgets.CDetailView');

class TDetailView extends CDetailView
{
	
	/**
	 * Initialize cssFile
	 */
	public function init()
	{
		$this->cssFile = Yii::app()->theme->baseUrl . '/css/tdetailview.css';
		parent::init();
	}
	
}


?>
